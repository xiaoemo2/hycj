package com.hooview.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmsConfig {
    @Value("${sms.accessKeyId}")
    private String accessKeyId;
    @Value("${sms.accessKeySecret}")
    private String accessKeySecret;
    @Value("${sms.endpoint}")
    private String endpoint;
    @Value("${sms.regionld}")
    private String regionld;
    @Value("${sms.product}")
    private String product;
    @Value("${sms.domain}")
    private String domain;
    @Value("${sms.signName}")
    private String signName;
    @Value("${sms.templateCode-verifyCode}")
    private String templVerifyCode;

    public String getTemplVerifyCode() {
        return templVerifyCode;
    }

    public void setTemplVerifyCode(String templVerifyCode) {
        this.templVerifyCode = templVerifyCode;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRegionld() {
        return regionld;
    }

    public void setRegionld(String regionld) {
        this.regionld = regionld;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }
}
