package com.hooview.api.dto;

import com.hooview.api.entity.User;

import javax.swing.text.StyledEditorKit;
import java.util.Date;

public class UserDTO {
    private String uid;
    private String displayName;
    private String avatar;
    private Date lastSignTime;
    private String hyNum;
    private String mobile;
    private String openId;
    private Float score;
    private Float hyCoin;
    private String content;
    private String media;
    private String status;
    private String isSign;
    private Integer inviteNum = 0;
    private Date createTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getInviteNum() {
        return inviteNum;
    }

    public void setInviteNum(Integer inviteNum) {
        this.inviteNum = inviteNum;
    }



    public UserDTO() {
    }

    public UserDTO(String uid, String displayName, String avatar, Date lastSignTime, String hyNum) {
        this.uid = uid;
        this.displayName = displayName;
        this.avatar = avatar;
        this.lastSignTime = lastSignTime;
        this.hyNum = hyNum;

    }

    public UserDTO(User user){
        this.uid = user.getId();
        this.displayName = user.getDisplayName();
        this.avatar = user.getAvatar();
        this.lastSignTime = user.getLastSignTime();
        this.hyNum = user.getHyNum();
        this.mobile = user.getMobile();
        this.openId = user.getOpenId();
        this.score = user.getScore();
        this.hyCoin = user.getHyCoin();
        this.content = user.getContent();
        this.media = user.getMedia();
        this.status = user.getStatus();
        this.createTime = user.getCreateTime();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getLastSignTime() {
        return lastSignTime;
    }

    public void setLastSignTime(Date lastSignTime) {
        this.lastSignTime = lastSignTime;
    }

    public String getHyNum() {
        return hyNum;
    }

    public void setHyNum(String hyNum) {
        this.hyNum = hyNum;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public Float getHyCoin() {
        return hyCoin;
    }

    public void setHyCoin(Float hyCoin) {
        this.hyCoin = hyCoin;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsSign() {
        return isSign;
    }

    public void setIsSign(String isSign) {
        this.isSign = isSign;
    }
}
