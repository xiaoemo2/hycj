package com.hooview.api.dto;

import com.hooview.api.entity.ProfitRecord;

import java.util.Date;

public class ProfitRecordDTO {
    private String id;

    private Date createTime;

    private Date updateTime;

    private String type;

    private Double num;

    private String uid;

    private Integer newsId;

    private  String avatar;

    private String avatar_1;

    private String avatar_2;

    private String displayName;

    private String displayName_1;

    private String displayName_2;


    public ProfitRecordDTO() {
    }
    public ProfitRecordDTO(ProfitRecord profitRecord) {
        this.id = profitRecord.getId();
        this.createTime = profitRecord.getCreateTime();
        this.updateTime = profitRecord.getUpdateTime();
        this.type = profitRecord.getType();
        this.num = profitRecord.getNum();
        this.uid = profitRecord.getUid();
        this.newsId = profitRecord.getNewsId();
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName_1() {
        return displayName_1;
    }

    public void setDisplayName_1(String displayName_1) {
        this.displayName_1 = displayName_1;
    }

    public String getDisplayName_2() {
        return displayName_2;
    }

    public void setDisplayName_2(String displayName_2) {
        this.displayName_2 = displayName_2;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getAvatar_1() {
        return avatar_1;
    }

    public void setAvatar_1(String avatar_1) {
        this.avatar_1 = avatar_1;
    }

    public String getAvatar_2() {
        return avatar_2;
    }

    public void setAvatar_2(String avatar_2) {
        this.avatar_2 = avatar_2;
    }

}
