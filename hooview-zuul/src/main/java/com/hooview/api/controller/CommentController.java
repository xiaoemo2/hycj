package com.hooview.api.controller;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Comment;
import com.hooview.api.entity.News;
import com.hooview.api.service.CommentService;
import com.hooview.api.service.NewsService;
import com.hooview.api.service.UserService;
import com.hooview.common.utils.AjaxMsg;
import com.hooview.common.utils.JpushClient;
import com.hooview.common.utils.ValidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 评论相关
 *
 * @author lee
 * @email
 * @date 2018-01-19 09:34:44
 */
@RestController
public class CommentController {
    private static Logger logger = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    private NewsService newsService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;


    /**
     * 点赞
     */
    @RequestMapping(value = "/api/comment/like", method = RequestMethod.POST)
    public AjaxMsg like(String uid, Long commentId) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        if (ValidUtil.isEmpty(uid) || commentId == 0) {
            ajaxMsg.setCode(AjaxMsg.FAILURE);
            ajaxMsg.setMessage("数据不能为空");
            return ajaxMsg;
        }
        commentService.like(uid, commentId);
        ajaxMsg.setCode(AjaxMsg.SUCCESS);
        return ajaxMsg;
    }

    /**
     * 评论
     */
    @RequestMapping(value = "/api/news/comment", method = RequestMethod.POST)
    public AjaxMsg comment(String uid, Long newsId, Long commentId, String content) {

        AjaxMsg ajaxMsg = new AjaxMsg();
        if (ValidUtil.isEmpty(uid)) {
            ajaxMsg.setCode(AjaxMsg.FAILURE);
            ajaxMsg.setMessage("用戶不能為空");
            return ajaxMsg;
        }
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setNewsId(newsId);
        comment.setUserId(uid);
        if (commentId!=null&&commentId != 0) {
            comment.setParentId(commentId);
        }
        commentService.insert(comment);

        //评论完成后 消息通知对方
        News news = newsService.selectByPrimaryKey(Math.toIntExact(newsId));
        try {
            if (commentId != 0) {
                Comment c = commentService.selectByPrimaryKey(commentId);
                JpushClient.sendPush(news.getTitle(),content,2,c.getUserId());
            } else {
                JpushClient.sendPush(news.getTitle(),content,2,news.getAuthor());
            }
        }catch (Exception e){
            logger.info("消息通知失败！");
            e.printStackTrace();
        }
        return ajaxMsg;
    }

    /**
     * 互动消息列表
     */
    @RequestMapping(value = "/api/comments/list",method = RequestMethod.POST)
    public AjaxMsg commentsList(String uid, @RequestParam(defaultValue = "1") Integer pageNum , @RequestParam(defaultValue = "10") Integer pageSize){
        List<Map<String, Object>> result = commentService.findByUser(uid,pageNum,pageSize);
        return new AjaxMsg(result);
    }

    /**
     * 评论列表
     */
    @RequestMapping(value = "/api/news/comments", method = RequestMethod.POST)
    public AjaxMsg comments(String uid, Long newsId,int pageSize,int pageNo) {

        AjaxMsg ajaxMsg = new AjaxMsg();
        Map map = new HashMap();
        if (!ValidUtil.isEmpty(uid)) {
            map.put("userId", uid);
        }
        map.put("newsId", newsId);
        map.put("pageSize", pageSize);
        map.put("pageNo", pageNo);

        Page<Map<String, Object>> comments = commentService.findMapByPage(map);
        ajaxMsg.setCode(AjaxMsg.SUCCESS);
        ajaxMsg.setDatas(comments);
        return ajaxMsg;
    }
}
