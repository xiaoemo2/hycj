package com.hooview.api.controller;

import com.hooview.api.service.UploadService;
import com.hooview.common.aliyunoss.OSSClientUtil;
import com.hooview.common.utils.AjaxMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/upload")
public class FileUploadController {

    @Autowired
    private UploadService uploadService;
    @Autowired
    private OSSClientUtil ossClientUtil;


    /**
     * 头像上传
     * @param file
     * @return
     */
    @RequestMapping(value = "/file",method = RequestMethod.POST)
    @ResponseBody
    public AjaxMsg uploadAvatar(MultipartFile file) throws Exception {
        String url = ossClientUtil.uploadImg2Oss(file);
        return new AjaxMsg(url);
    }
}
