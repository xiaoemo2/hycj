package com.hooview.api.controller;

import com.hooview.api.entity.News;
import com.hooview.api.service.NewsService;
import com.hooview.common.utils.AjaxMsg;
import com.hooview.common.utils.JpushClient;
import com.hooview.common.utils.ValidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class JPushController {
    @Autowired
    private NewsService newsService;
    /**
     * 快讯推送通知 微博消息推送
     * @param id
     */
    @RequestMapping(value = "/jpush",method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg push(Integer id ) {
        if(ValidUtil.isEmpty(id)) return new AjaxMsg(AjaxMsg.FAILURE,"参数不能为空!");
        News news =  newsService.selectByPrimaryKey(id);
        if(news == null)
            return new AjaxMsg(AjaxMsg.FAILURE,"推送内容不存在!");
        //快讯
        if (news.getChannelId() == 1)
            JpushClient.sendPush(news.getTitle(), news.getContent(), 0, null);
        //微博
        if(news.getChannelId() == 2)
            JpushClient.sendPush(news.getTitle(), news.getContent(), 1, null);
        return new AjaxMsg();
    }
}
