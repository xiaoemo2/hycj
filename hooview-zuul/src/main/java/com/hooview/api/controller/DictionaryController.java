package com.hooview.api.controller;

import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.DictionaryService;
import com.hooview.common.utils.AjaxMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * banner表
 *
 * @author lee
 * @email
 * @date 2018-01-19 09:34:44
 */
@RestController
public class DictionaryController {
    @Autowired
    private DictionaryService dictionaryService;


    /**
     * news 详细
     */
    @RequestMapping(value = "/api/setting/dictionary", method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg queryNewsById(@RequestParam String keyNo) {
        List<Dictionary> dictionaries = dictionaryService.getDicByKeyNo(keyNo);
        return new AjaxMsg(dictionaries);
    }

}
