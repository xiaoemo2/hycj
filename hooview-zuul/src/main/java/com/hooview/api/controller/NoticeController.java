package com.hooview.api.controller;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Notices;
import com.hooview.api.entity.User;
import com.hooview.api.service.NoticeService;
import com.hooview.api.service.UserService;
import com.hooview.common.utils.AjaxMsg;
import com.hooview.contants.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notice")
public class NoticeController {

    @Autowired
    private NoticeService noticeService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/system",method = RequestMethod.GET)
    public AjaxMsg getSystemNotice(String uid, @RequestParam(defaultValue = "1") Integer pageNum,@RequestParam(defaultValue = "10") Integer pageSize){
        User user = userService.queryById(uid);
        if(Constants.USER_MEDIA_NO.equals(user.getMedia())) {
            Page<Notices> page = noticeService.findByType(Constants.SYSYTEM_NOTCIE_ORDINARY, pageNum, pageSize);
            if(page == null)
                return new AjaxMsg();
            return new AjaxMsg(page.getResult());
        }
        if(Constants.USER_MEDIA_YES.equals(user.getMedia())) {
            Page<Notices> page = noticeService.findByType(Constants.SYSYTEM_NOTCIE_MEDIA, pageNum, pageSize);
            if(page == null)
                return new AjaxMsg();
            return new AjaxMsg(page.getResult());
        }
        return new AjaxMsg();
    }
}
