package com.hooview.api.controller;

import com.hooview.api.entity.SignRule;
import com.hooview.api.service.SignRuleService;
import com.hooview.common.utils.AjaxMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springside.modules.utils.base.annotation.NotNull;

import java.util.List;

@RestController
@RequestMapping("/api/sign")
public class SignRuleController {
    @Autowired
    private SignRuleService signRuleService;

    @RequestMapping(value = "/signRuleInfo",method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg signRuleInfo(){
        List<SignRule> list = signRuleService.findAll();
        return new AjaxMsg(list);
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @ResponseBody
    public AjaxMsg save(@NotNull Integer signDays,@NotNull Integer num ){
        SignRule signRule = new SignRule();
        signRule.setSignDays(signDays);
        signRule.setRewardCoins(num);
        signRuleService.save(signRule);
        return new AjaxMsg();
    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg delete(@NotNull String id){
        signRuleService.delete(id);
        return new AjaxMsg();
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public AjaxMsg delete(@RequestBody SignRule signRule){
        signRuleService.update(signRule);
        return new AjaxMsg();
    }

    @RequestMapping(value = "/findById",method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg findById(@NotNull String id){
        SignRule signRule = signRuleService.findById(id);
        return new AjaxMsg();
    }
}
