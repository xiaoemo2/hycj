package com.hooview.api.controller;

import com.hooview.api.service.BannerService;
import com.hooview.common.utils.AjaxMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * banner表
 * 
 * @author lee
 * @email 
 * @date 2018-01-19 09:34:44
 */
@RestController
@RequestMapping("/api/banner")
public class BannerController {
	@Autowired
	private BannerService bannerService;

	/**
	 * 头条："01"
	 * 获取banner
	 */
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	@ResponseBody
	public AjaxMsg info(int channelId){
		Map<String,Object> map=new HashMap<>();
		map.put("type", channelId);
		map.put("status","01");
		List list=bannerService.queryList(map);
		return  new AjaxMsg(list);
	}

}
