package com.hooview.api.controller;

import com.github.pagehelper.Page;
import com.hooview.api.entity.News;
import com.hooview.api.entity.Ticker;
import com.hooview.api.service.TickerService;
import com.hooview.common.utils.AjaxMsg;
import com.hooview.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * banner表
 *
 * @author lee
 * @email
 * @date 2018-01-19 09:34:44
 */
@RestController
public class TickerController {
    @Autowired
    private TickerService tickerService;

    /**
     * 当前币种的价格信息
     */
    @RequestMapping(value = "/api/market/currentMarketData", method = RequestMethod.POST)
    @ResponseBody
    public AjaxMsg detail(String coinCode) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        Map res = new HashMap();
        DecimalFormat df = new DecimalFormat("######0.0000");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map<String, Object> params = new HashMap();
        params.put("coinCode", coinCode);
        params.put("limit", 1);
        params.put("sidx", "id");
        params.put("sort", "DESC");
        Map result = new HashMap();
        //查詢最新數據
        List<Ticker> tickers = tickerService.queryByParams(params);
        if (tickers != null) {
            Ticker ticker = tickers.get(0);


            //查询当天第一条数据
            Map<String, Object> params2 = new HashMap();
            params2.put("coinCode", coinCode);
            params2.put("limit", 1);
            params2.put("sidx", "id");
            params2.put("sort", "ASC");
            params2.put("date", sdf.format(new Date()));
            List<Ticker> tickers2 = tickerService.queryByParams(params2);
            Ticker ticker2 = tickers2.get(0);
            Double rate = (ticker.getLast() - ticker2.getLast()) / ticker.getLast();

            result.put("siteName", ticker.getSiteName());
            result.put("last", ticker.getLast());
            result.put("buy", ticker.getBuy());
            result.put("sell", ticker.getSell());
            result.put("high", ticker.getHigh());
            result.put("lastUsd", ticker.getLastUsd());
            result.put("low", ticker.getLow());
            result.put("volume", ticker.getVol());
            result.put("coinCode", ticker.getCoinCode());
            result.put("rate", df.format(rate));
        }

        res.put("curent", result);


        //趋势
        Map<String, Object> trend_params = new HashMap();
        trend_params.put("coinCode", coinCode);
        trend_params.put("startTime", DateUtils.getPreHoursStr(24));
        trend_params.put("endTime", DateUtils.formatDate(new Date(), DateUtils.DATE_TIME_PATTERN));
        List<Ticker> trend_tickers = tickerService.queryTrend(trend_params);
        ajaxMsg.setCode(AjaxMsg.SUCCESS);
        res.put("trend", trend_tickers);

        //币站信息



        ajaxMsg.setDatas(res);
        return ajaxMsg;
    }

}
