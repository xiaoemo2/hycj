package com.hooview.api.service.impl;

import com.hooview.api.entity.ReadHistory;
import com.hooview.api.service.ReadHistoryService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ReadHistoryServiceBack implements ReadHistoryService {
    @Override
    public void insert(String uid, Integer newsId) {

    }

    @Override
    public ReadHistory selectByNewsIdAndUid(Integer newsId, String uid) {
        return null;
    }

    @Override
    public void deleteById(Integer id) {

    }
}
