package com.hooview.api.service;

import com.hooview.api.entity.Follows;
import com.hooview.api.service.impl.FollowsServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = FollowsServiceBack.class)
public interface FollowsService {
    @RequestMapping(value = "/follow/selectByUidAndAutorId",method = RequestMethod.GET)
    Follows selectByUidAndAutorId(@RequestParam("uid") String uid,@RequestParam("authorId") String authorId);

    @RequestMapping(value = "/follow/insert",method = RequestMethod.GET)
    void insert(@RequestParam("uid") String uid,@RequestParam("authorId") String authorId);

    @RequestMapping(value = "/follow/deleteById",method = RequestMethod.GET)
    void deleteById(@RequestParam("id") Integer id);
}
