package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hooview.api.entity.User;
import com.hooview.api.entity.dto.AuthorSimpleInfo;
import com.hooview.api.entity.dto.UserDTO;
import com.hooview.api.service.impl.UserServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(value = "hooview-service",fallback = UserServiceBack.class)
public interface UserService {
    @RequestMapping(value = "/api/user/register",method = RequestMethod.POST)
    void register(@RequestBody User user);
    @RequestMapping(value = "/api/user/queryByUsername",method = RequestMethod.GET)
    User queryByUsername(@RequestParam("username") String username);
    @RequestMapping(value = "/api/user/queryById",method = RequestMethod.GET)
    User queryById(@RequestParam("uid") String uid);
    @RequestMapping(value = "/api/user/createToken",method = RequestMethod.POST)
    String createToken(@RequestBody User user);
    @RequestMapping(value = "/api/user/update",method = RequestMethod.POST)
    void update(@RequestBody User user);
    @RequestMapping(value = "/api/user/logout",method = RequestMethod.GET)
    void logout(@RequestParam("token") String token);
    @RequestMapping(value = "/api/user/queryByOpenId",method = RequestMethod.GET)
    User queryByOpenId(@RequestParam("openId") String openId);
    @RequestMapping(value = "/api/user/queryByParams",method = RequestMethod.POST)
    Map queryByParams(@RequestBody Map<String,Object> params, @RequestParam(name = "pageNum") Integer pageNum, @RequestParam(name = "pageSize") Integer pageSize);
    @RequestMapping(value = "/api/user/queryMediaUser",method = RequestMethod.POST)
    Map queryMediaUserByParams(@RequestBody Map<String, Object> params,@RequestParam(name = "pageNum") Integer pageNum,@RequestParam(name = "pageSize") Integer pageSize);
    @RequestMapping(value = "/api/user/queryAuthorInfo",method = RequestMethod.GET)
    AuthorSimpleInfo queryAuthorInfo(@RequestParam("authorId") String authorId);
    @RequestMapping(value = "/api/user/queryInvitedFriends",method = RequestMethod.GET)
    Page<User> queryInvitedFriends(@RequestParam("uid") String uid, @RequestParam("type") String type, @RequestParam("pageNum") Integer pageNum, @RequestParam(name = "pageSize") Integer pageSize);
    @RequestMapping(value = "/api/user/queryTodayEnergyPoolInfo",method = RequestMethod.GET)
    Map<String,Object> queryTodayEnergyPoolInfo(@RequestParam("uid") String uid);
    @RequestMapping(value = "/api/user/countInvite",method = RequestMethod.GET)
    Integer countInvite(@RequestParam("uid") String uid);

    @RequestMapping(value = "/api/user/recommendAuthor",method = RequestMethod.POST)
    Map recommendAuthor(@RequestBody Map<String, Object> params,@RequestParam(name = "pageNum") Integer pageNum,@RequestParam(name = "pageSize") Integer pageSize);

}
