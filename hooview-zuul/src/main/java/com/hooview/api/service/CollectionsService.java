package com.hooview.api.service;

import com.hooview.api.entity.Collections;
import com.hooview.api.service.impl.CollectionsServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = CollectionsServiceBack.class)
public interface CollectionsService {

    @RequestMapping(value = "/collections/selectByUidAndNewsId",method = RequestMethod.GET)
    Collections selectByUidAndNewsId(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/collections/insert",method = RequestMethod.GET)
    void insert(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/collections/deleteById",method = RequestMethod.GET)
    void deleteById(@RequestParam("id") Integer id);
}
