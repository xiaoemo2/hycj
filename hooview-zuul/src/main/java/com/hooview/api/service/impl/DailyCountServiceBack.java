package com.hooview.api.service.impl;

import com.hooview.api.entity.DailyCount;
import com.hooview.api.service.DailyCountService;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class DailyCountServiceBack implements DailyCountService {
    @Override
    public DailyCount selectToday(String uid) {
        return null;
    }

    @Override
    public void insert(DailyCount dailyCount) {

    }

    @Override
    public void updateByPrimaryKeySelective(DailyCount dailyCount) {

    }
}
