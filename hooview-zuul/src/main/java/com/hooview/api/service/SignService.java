package com.hooview.api.service;

import com.hooview.api.entity.Sign;
import com.hooview.api.service.impl.SignServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = SignServiceBack.class)
public interface SignService {
    /**
     * 获取用户最近一次签到
     * @param uid
     * @return
     */
    @RequestMapping(value = "/sign/queryHead1",method = RequestMethod.GET)
    Sign queryHead1ByUser(@RequestParam("uid") String uid);

    /**
     * 清空用户连续签到次数
     * @param sign
     */
    @RequestMapping(value = "/sign/clearSignTimes",method = RequestMethod.POST)
    void clearSignTimes(@RequestBody Sign sign);

    /**
     * 签到保存
     * @param sign
     */
    @RequestMapping(value = "/sign/save",method = RequestMethod.POST)
    void save(@RequestBody Sign sign);
}
