package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.hooview.api.entity.News;
import com.hooview.api.service.NewsService;
import com.hooview.common.utils.QueryFilters;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public class NewsServiceBack implements NewsService {


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(News record) {
        return 0;
    }

    @Override
    public News selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(News record) {
        return 0;
    }

    @Override
    public List<News> queryByParams(Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<News> getByFilter(QueryFilters filters) {
        return null;
    }

    @Override
    public Integer queryNewsCount(@RequestBody Map<String, Object> map) {
        return null;
    }

    @Override
    public Page<News> findByPage(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> findMapByPage(@RequestBody Map<String, Object> params) {
        return null;
    }
    @Override
    public Page<Map<String, Object>> findFollowMapByPage(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public Map<String, Object> selectMapByPrimaryKey(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> queryHistoriesByPage(@RequestBody Map<String, Object> params) {
        return null;
    }
    @Override
    public Page<Map<String, Object>> queryHistoriesByPageNo(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> queryCollectionsByPage(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public Page<Map<String, Object>> queryAuthorByPage(@RequestBody Map<String, Object> params) {
        return null;
    }
    @Override
    public Page<Map<String, Object>> queryRecommendsByPage(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public List<Map<String, Object>> findByKeyword(String keyword, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public Map<String, Object> findNextNews(Integer currentId) {
        return null;
    }

    @Override
    public List<Map<String, Object>> findRecommendNews(Integer currentId) {
        return null;
    }


}
