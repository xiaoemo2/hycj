package com.hooview.api.service.impl;

import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.DictionaryService;
import com.hooview.common.utils.QueryFilters;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DictionaryServiceBack implements DictionaryService {

    @Override
    public List<Dictionary> getByFilter(QueryFilters filters) {
        return null;
    }

    @Override
    public Integer update(Dictionary entity) {
        return null;
    }

    @Override
    public Dictionary insert(Dictionary entity) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<Dictionary> getDicByKeyNo(String keyNo) {
        return null;
    }
}
