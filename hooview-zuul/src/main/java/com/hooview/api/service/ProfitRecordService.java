package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.ProfitRecord;
import com.hooview.api.service.impl.ProfitRecordServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "hooview-service",fallback = ProfitRecordServiceBack.class)
public interface ProfitRecordService {
    @RequestMapping(value = "/profitRecord/queryByUser",method = RequestMethod.GET)
    List<ProfitRecord> queryByUser(@RequestParam("uid") String uid, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);

    @RequestMapping(value = "/profitRecord/profitToday",method = RequestMethod.GET)
    Float profitToday(@RequestParam("uid") String uid);

    @RequestMapping(value = "/profitRecord/profitSum",method = RequestMethod.GET)
    Double profitSum(@RequestParam("uid") String uid);

    @RequestMapping(value = "/profitRecord/save",method = RequestMethod.POST)
    void save(@RequestBody ProfitRecord profitRecord);

    @RequestMapping(value = "/profitRecord/update",method = RequestMethod.POST)
    void update(@RequestBody ProfitRecord profitRecord);

    @RequestMapping(value = "/profitRecord/delete",method = RequestMethod.GET)
    void delete(@RequestParam("id") String id);

    @RequestMapping(value = "/profitRecord/countByUser",method = RequestMethod.GET)
    Integer countByUser(@RequestParam("uid") String uid);

    @RequestMapping(value = "/profitRecord/findByUserAndNewsId",method = RequestMethod.GET)
    ProfitRecord findByUserAndNewsId(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/profitRecord/getProfit",method = RequestMethod.GET)
    Double getProfit(@RequestParam("uid") String uid,@RequestParam("type") String type,@RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/profitRecord/findByUserAndNewsIdAndType",method = RequestMethod.GET)
    ProfitRecord findByUserAndNewsIdAndType(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId,@RequestParam("type") String type);
}
