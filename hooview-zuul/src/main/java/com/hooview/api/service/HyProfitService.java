package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyProfit;
import com.hooview.api.service.impl.HyProfitServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = HyProfitServiceBack.class)
public interface HyProfitService {
    @RequestMapping(value = "/hyProfit/selectByUser",method = RequestMethod.GET)
    Page<HyProfit> selectByUser(@RequestParam("uid") String uid,@RequestParam("pageNum") Integer pageNum,@RequestParam("pageSize") Integer pageSize);
}
