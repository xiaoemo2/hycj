package com.hooview.api.service.impl;

import com.hooview.api.entity.Sign;
import com.hooview.api.service.SignService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
public class SignServiceBack implements SignService {
    @Override
    public Sign queryHead1ByUser(String uid) {
        return null;
    }

    @Override
    public void clearSignTimes(@RequestBody Sign sign) {

    }

    @Override
    public void save(Sign sign) {

    }
}
