package com.hooview.api.service;

import com.hooview.api.service.impl.VerifyCodeServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = VerifyCodeServiceBack.class)
public interface VerifyCodeService {
    @RequestMapping(value = "/verifyCode/genCode",method = RequestMethod.GET)
    String genCode(@RequestParam("mobile") String mobile);

    @RequestMapping(value = "/verifyCode/isValidCode",method = RequestMethod.GET)
    Boolean isValidCode(@RequestParam("mobile") String mobile,@RequestParam("code") String code);

    @RequestMapping(value = "/verifyCode/deleteCode",method = RequestMethod.GET)
    void deleteCode(@RequestParam("mobile") String mobile);
}
