package com.hooview.api.service.impl;

import com.hooview.api.entity.SignRule;
import com.hooview.api.service.SignRuleService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SignRuleServiceBack implements SignRuleService {
    @Override
    public List<SignRule> findAll() {
        return null;
    }

    @Override
    public void save(SignRule signRule) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public void update(SignRule signRule) {

    }

    @Override
    public SignRule findById(String id) {
        return null;
    }

    @Override
    public SignRule findBySigndays(int signDays) {
        return null;
    }
}
