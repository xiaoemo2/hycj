package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hooview.api.entity.User;
import com.hooview.api.entity.dto.AuthorSimpleInfo;
import com.hooview.api.entity.dto.UserDTO;
import com.hooview.api.service.UserService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UserServiceBack implements UserService {
    @Override
    public void register(User user) {

    }
    @Override
    public User queryByUsername(String username) {
        return null;
    }

    @Override
    public User queryById(String uid) {
        return null;
    }

    @Override
    public String createToken(User user) {
        return null;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void logout(String token) {

    }

    @Override
    public User queryByOpenId(String openId) {
        return null;
    }

    @Override
    public Map queryByParams(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public Map queryMediaUserByParams(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public AuthorSimpleInfo queryAuthorInfo(String authorId) {
        return null;
    }

    @Override
    public Page<User> queryInvitedFriends(String uid, String type, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public Map<String, Object> queryTodayEnergyPoolInfo(String uid) {
        return null;
    }

    @Override
    public Integer countInvite(String uid) {
        return null;
    }

    @Override
    public Map recommendAuthor(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        return null;
    }

}
