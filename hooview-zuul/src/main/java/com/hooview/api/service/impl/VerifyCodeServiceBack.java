package com.hooview.api.service.impl;

import com.hooview.api.service.VerifyCodeService;
import org.springframework.stereotype.Component;

@Component
public class VerifyCodeServiceBack implements VerifyCodeService {
    @Override
    public String genCode(String mobile) {
        return null;
    }

    @Override
    public Boolean isValidCode(String mobile, String code) {
        return false;
    }

    @Override
    public void deleteCode(String mobile) {

    }
}
