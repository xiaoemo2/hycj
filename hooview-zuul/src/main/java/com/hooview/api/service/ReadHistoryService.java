package com.hooview.api.service;

import com.hooview.api.entity.ReadHistory;
import com.hooview.api.service.impl.ReadHistoryServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = ReadHistoryServiceBack.class)
public interface ReadHistoryService {
    @RequestMapping(value = "/readHistory/insert",method = RequestMethod.GET)
    void insert(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/readHistory/selectByNewsIdAndUid",method = RequestMethod.GET)
    ReadHistory selectByNewsIdAndUid(@RequestParam("newsId") Integer newsId, @RequestParam("uid") String uid);

    @RequestMapping(value = "/readHistory/deleteById",method = RequestMethod.GET)
    void deleteById(@RequestParam("id") Integer id);
}
