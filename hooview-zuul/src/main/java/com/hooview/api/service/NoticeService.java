package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Notices;
import com.hooview.api.service.impl.NoticeServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service", fallback = NoticeServiceBack.class)
public interface NoticeService {

    @RequestMapping(value = "/notice/findByType",method = RequestMethod.GET)
    public Page<Notices> findByType(@RequestParam("type") String type, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);
}
