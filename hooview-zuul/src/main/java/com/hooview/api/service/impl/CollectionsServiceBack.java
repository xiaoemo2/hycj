package com.hooview.api.service.impl;

import com.hooview.api.entity.Collections;
import com.hooview.api.service.CollectionsService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class CollectionsServiceBack implements CollectionsService{
    @Override
    public Collections selectByUidAndNewsId(String uid, Integer newsId) {
        return null;
    }

    @Override
    public void insert(String uid, Integer newsId) {

    }

    @Override
    public void deleteById(Integer id) {

    }
}
