package com.hooview.api.service;

import com.hooview.api.service.impl.UploadServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(value = "hooview-service" ,fallback = UploadServiceBack.class)
public interface UploadService {

    @RequestMapping(value = "/upload/avatar",method = RequestMethod.POST)
    String uploadAvatar(MultipartFile file);
}
