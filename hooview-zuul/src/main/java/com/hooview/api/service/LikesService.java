package com.hooview.api.service;

import com.hooview.api.entity.Likes;
import com.hooview.api.service.impl.LikesServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service",fallback = LikesServiceBack.class)
public interface LikesService {
    @RequestMapping(value = "/likes/insert",method = RequestMethod.GET)
    void insert(@RequestParam("userId") String userId,@RequestParam("newsId") Integer newsId,@RequestParam("type") String type);

    @RequestMapping(value = "/likes/selectByUserIdAndNewsId",method = RequestMethod.GET)
    Likes selectByUserIdAndNewsId(@RequestParam("userId") String userId, @RequestParam("newsId") Integer newsId);
}
