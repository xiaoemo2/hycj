package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Ticker;
import com.hooview.api.service.impl.TickerServiceBack;
import com.hooview.common.utils.QueryFilters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 快讯、微博、头条
 *
 * @author lee
 * @email
 * @date 2018-01-19 09:52:55
 */
@FeignClient(value = "hooview-service", fallback = TickerServiceBack.class)
public interface TickerService {
    @RequestMapping(value = "/ticker/queryTrend", method = RequestMethod.POST)
    List<Ticker> queryTrend(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/ticker/queryByParams", method = RequestMethod.POST)
    List<Ticker> queryByParams(@RequestBody Map<String, Object> map);
}
