package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.News;
import com.hooview.api.service.impl.NewsServiceBack;
import com.hooview.common.utils.QueryFilters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 快讯、微博、头条
 *
 * @author lee
 * @email
 * @date 2018-01-19 09:52:55
 */
@FeignClient(value = "hooview-service", fallback = NewsServiceBack.class)
public interface NewsService {
    @RequestMapping(value = "/news/delete", method = RequestMethod.POST)
    int deleteByPrimaryKey(Integer id);

    @RequestMapping(value = "/news/save", method = RequestMethod.POST)
    int insert(@RequestBody News record);

    @RequestMapping(value = "/news/queryObject", method = RequestMethod.GET)
    News selectByPrimaryKey(@RequestParam("id") Integer id);

    @RequestMapping(value = "/news/update", method = RequestMethod.POST)
    int updateByPrimaryKeySelective(@RequestBody News record);

    @RequestMapping(value = "/news/queryList", method = RequestMethod.POST)
    List<News> queryByParams(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/news/getByFilter", method = RequestMethod.POST)
    Page<News> getByFilter(@RequestBody QueryFilters filters);

    @RequestMapping(value = "/news/queryNewsCount", method = RequestMethod.GET)
    Integer queryNewsCount(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/news/findByPage", method = RequestMethod.POST)
    Page<News> findByPage(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/findMapByPage", method = RequestMethod.POST)
    Page<Map<String,Object>> findMapByPage(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/findFollowMapByPage", method = RequestMethod.POST)
    Page<Map<String,Object>> findFollowMapByPage(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/selectMapByPrimaryKey", method = RequestMethod.POST)
    Map<String,Object> selectMapByPrimaryKey(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/queryHistoriesByPage", method = RequestMethod.POST)
    Page<Map<String,Object>> queryHistoriesByPage(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/queryHistoriesByPageNo", method = RequestMethod.POST)
    Page<Map<String,Object>> queryHistoriesByPageNo(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/queryCollectionsByPage", method = RequestMethod.POST)
    Page<Map<String,Object>> queryCollectionsByPage(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/news/queryAuthorByPage", method = RequestMethod.POST)
    Page<Map<String, Object>> queryAuthorByPage(@RequestBody Map<String, Object> params);
    @RequestMapping(value = "/news/queryRecommendsByPage", method = RequestMethod.POST)
    Page<Map<String,Object>> queryRecommendsByPage(@RequestBody Map<String, Object> params);

    @GetMapping("/news/findByKeyword")
    List<Map<String,Object>> findByKeyword(@RequestParam("keyword") String keyword,@RequestParam("pageNum") Integer pageNum,@RequestParam("pageSize") Integer pageSize);

    @GetMapping("/news/findNextNews")
    Map<String,Object> findNextNews(@RequestParam("currentId") Integer currentId);

    @GetMapping("/news/findRecommendNews")
    List<Map<String, Object>> findRecommendNews(@RequestParam("currentId") Integer currentId);
}
