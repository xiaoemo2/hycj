package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.hooview.api.entity.ProfitRecord;
import com.hooview.api.service.ProfitRecordService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProfitRecordServiceBack implements ProfitRecordService {
    @Override
    public List<ProfitRecord> queryByUser(String uid, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public Float profitToday(String uid) {
        return null;
    }

    @Override
    public Double profitSum(String uid) {
        return null;
    }

    @Override
    public void save(ProfitRecord profitRecord) {

    }

    @Override
    public void update(ProfitRecord profitRecord) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public Integer countByUser(String uid) {
        return null;
    }

    @Override
    public ProfitRecord findByUserAndNewsId(String uid, Integer newsId) {
        return null;
    }

    @Override
    public Double getProfit(String uid, String type, Integer newsId) {
        return null;
    }

    @Override
    public ProfitRecord findByUserAndNewsIdAndType(String uid, Integer newsId, String type) {
        return null;
    }

}
