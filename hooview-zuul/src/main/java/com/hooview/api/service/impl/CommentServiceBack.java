package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Comment;
import com.hooview.api.service.CommentService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@Component
public class CommentServiceBack implements CommentService {


    @Override
    public void delete(Long id) {
    }

    @Override
    public Long insert(Comment record) {
        return 0L;
    }

    @Override
    public Comment selectByPrimaryKey(Long id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(Comment record) {
        return 0;
    }

    @Override
    public Page<Map<String, Object>> findMapByPage(@RequestBody Map<String, Object> params) {
        return null;
    }

    @Override
    public void like(String uid, Long commentId) {

    }

    @Override
    public List<Map<String, Object>> findByUser(String uid, Integer pageNum, Integer pageSize) {
        return null;
    }


}
