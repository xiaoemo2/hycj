package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Ticker;
import com.hooview.api.service.TickerService;
import com.hooview.common.utils.QueryFilters;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@Component

public class TickerServiceBack implements TickerService {


    @Override
    public List<Ticker> queryByParams(@RequestBody Map<String, Object> map) {
        return null;
    }

    @Override
    public List<Ticker> queryTrend(@RequestBody Map<String, Object> map) {
        return null;
    }
}
