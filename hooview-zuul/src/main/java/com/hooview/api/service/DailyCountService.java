package com.hooview.api.service;

import com.hooview.api.entity.DailyCount;
import com.hooview.api.service.impl.DailyCountServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;

@FeignClient(value = "hooview-service",fallback = DailyCountServiceBack.class)
public interface DailyCountService {
    @RequestMapping(value = "/dailyCount/selectToday",method = RequestMethod.GET)
    DailyCount selectToday(@RequestParam("uid") String uid);

    @RequestMapping(value = "/dailyCount/insert",method = RequestMethod.POST)
    void insert(@RequestBody DailyCount dailyCount);

    @RequestMapping(value = "/dailyCount/updateByPrimaryKeySelective",method = RequestMethod.POST)
    void updateByPrimaryKeySelective(@RequestBody DailyCount dailyCount);
}
