package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Comment;
import com.hooview.api.service.impl.CommentServiceBack;
import com.hooview.api.service.impl.LikesServiceBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(value = "hooview-service",fallback = CommentServiceBack.class)
public interface CommentService {

    @RequestMapping(value = "/comment/save", method = RequestMethod.POST)
    Long insert(@RequestBody Comment record);

    @RequestMapping(value = "/comment/delete", method = RequestMethod.POST)
    void delete(@RequestBody Long id);

    @RequestMapping(value = "/comment/queryObject", method = RequestMethod.GET)
    Comment selectByPrimaryKey(@RequestParam("id") Long id);

    @RequestMapping(value = "/comment/update", method = RequestMethod.POST)
    int updateByPrimaryKeySelective(@RequestBody Comment record);

    @RequestMapping(value = "/comment/findMapByPage", method = RequestMethod.POST)
    Page<Map<String, Object>> findMapByPage(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/comment/like", method = RequestMethod.POST)
    void like(@RequestParam("uid")String uid, @RequestParam("commentId")Long commentId);

    @RequestMapping(value = "/comment/list", method = RequestMethod.GET)
    List<Map<String, Object>> findByUser(@RequestParam("uid")String uid, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);
}