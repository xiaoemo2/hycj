package com.hooview.common.utils;

import com.aliyun.oss.ServiceException;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 对应jqgrid查询条件
 */
public class QueryFilters {
    //    groupOps: [ { op: "AND", text: "所有" },    { op: "OR",  text: "任一" } ],
    private String groupOp = "AND";
    private String sidx;
    private String sord = "ASC";
    private Integer pageNo = 1;
    private Integer pageSize = 10;
    private List<QueryRule> rules = new ArrayList<>();

    public String getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(String groupOp) {
        this.groupOp = groupOp;
    }

    public List<QueryRule> getRules() {
        return rules;
    }

    public void setRules(List<QueryRule> rules) {
        this.rules = rules;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /*
    * 在使用queryfilter查询之前调用formart，将前台传递的查询条件格式化为数据库查询的条件
    * */
    public void format(Map<String, Object> keyMap) {
        if (rules != null && rules.size() > 0) {
            for (QueryRule rule : rules) {
                Object queryName = keyMap.get(rule.getField().trim());
                if (queryName != null) {
                    if (queryName instanceof String) {
                        rule.setField((String) queryName);
                    } else {
                        for (String qn : (String[]) queryName) {
                            rule.setField(qn);
                        }

                    }
                } else {
                    throw new ServiceException(queryName + "不是有效的查询条件！");
                }
            }
        }
        if (StringUtils.isNotEmpty(this.sidx)) {
            this.sidx = this.sidx.trim();
            StringBuilder sidxbuilder = new StringBuilder("");
            String[] sidxes = this.sidx.split(",");
            for (String sid : sidxes) {
                sid = sid.trim();
                if (StringUtils.isNotEmpty(sid)) {
                    String[] sids = sid.split(" ");
                    Object queryName = null;
                    String s_sort = "ASC";
                    if (sids.length == 2) {
                        queryName = keyMap.get(sids[0].trim());
                        s_sort = sids[1];
                    } else {
                        queryName = keyMap.get(sid.trim());
                        s_sort = this.sord;
                    }
                    if (queryName != null) {
                        if (queryName instanceof String) {
                            sidxbuilder.append((String) queryName);
                            sidxbuilder.append(" ");
                            sidxbuilder.append(s_sort);
                            sidxbuilder.append(",");
                        } else {
                            for (String qn : (String[]) queryName) {
                                sidxbuilder.append((String) qn);
                                sidxbuilder.append(" ");
                                sidxbuilder.append(s_sort);
                                sidxbuilder.append(",");
                            }
                        }
                    } else {
                        throw new ServiceException(queryName + "不是有效的查询条件！");
                    }
                }
            }
            if (sidxbuilder.length() > 0) this.sidx = sidxbuilder.substring(0, sidxbuilder.length() - 1);
            else this.sidx = null;
        }
    }
}
