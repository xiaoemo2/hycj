package com.hooview.common.utils;


/**
 * Created by Administrator on 2015/6/21.
 */
public class QueryRule {
    private String field;
    // odata: [{ oper:'eq', text:'等于'},{ oper:'ne', text:'不等'},{ oper:'lt', text:'小于'},{ oper:'le', text:'小于等于'},
    // { oper:'gt', text:'大于\u3000\u3000'},{ oper:'ge', text:'大于等于'},{ oper:'bw', text:'开始于'},
    // { oper:'bn', text:'不开始于'},{ oper:'in', text:'属于\u3000\u3000'},{ oper:'ni', text:'不属于'},
    // { oper:'ew', text:'结束于'},{ oper:'en', text:'不结束于'},{ oper:'cn', text:'包含\u3000\u3000'},
    // { oper:'nc', text:'不包含'},{ oper:'nu', text:'不存在'},{ oper:'nn', text:'存在'}],
    private String data;
    private String op="cn";
    private String groupOp="AND";

    public QueryRule(){}
    public QueryRule(String filed,String data,String op,String groupOp){
        this.field=filed;
        this.data=data;
        this.op=op;
        this.groupOp=groupOp;
    }
    public String getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(String groupOp) {
        this.groupOp = groupOp;
    }
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOp() {
        String op_="like";
        if(op.equals("eq")){op_="=";}
        else if(op.equals("ne")){op_="!=";}
        else if(op.equals("lt")){op_="<";}
        else if(op.equals("le")){op_="<=";}
        else if(op.equals("gt")){op_=">";}
        else if(op.equals("ge")){op_=">=";}
        else if(op.equals("bw")){op_=" like ";}
        else if(op.equals("bn")){op_=" not like ";}
        else if(op.equals("in")){op_=" in ";}
        else if(op.equals("ni")){op_=" not in ";}
        else if(op.equals("ew")){op_=" like ";}
        else if(op.equals("en")){op_=" not like ";}
        else if(op.equals("cn")){op_=" like ";}
        else if(op.equals("nc")){op_=" not like ";}
        else if(op.equals("nu")){op_=" is null ";}
        else if(op.equals("nn")){op_=" is not null ";}
        else if(op.equals("in_s")){op_=" in (" + data + ")";}//特殊sql
        return op_;
    }

    public void setOp(String op) {
        this.op=op;
    }

    public String getData() {
        String data_=data;
        if(op.equals("eq")){data_=data;}
        else if(op.equals("ne")){data_=data;}
        else if(op.equals("lt")){data_=data;}
        else if(op.equals("le")){data_=data;}
        else if(op.equals("gt")){data_=data;}
        else if(op.equals("ge")){data_=data;}
        else if(op.equals("bw")){data_=data+"%";}
        else if(op.equals("bn")){data_=data+"%";}
        else if(op.equals("in")){data_="("+data+")";}
        else if(op.equals("ni")){data_="("+data+")";}
        else if(op.equals("ew")){data_="%"+data;}
        else if(op.equals("en")){data_="%"+data;}
        else if(op.equals("cn")){data_="%"+data+"%";}
        else if(op.equals("nc")){data_="%"+data+"%";}
        else if(op.equals("nu")){data_=null;}
        else if(op.equals("nn")){data_=null;}
        else if(op.equals("in_s")){data_=null;}
        return data_;
    }

    public void setData(String data) {
        this.data = data;
    }

}
