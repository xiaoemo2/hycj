package com.hooview.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 日期处理
 *
 * @author lee
 * @email sunlightcs@gmail.com
 * @date 2016年12月21日 下午12:53:33
 */
public class DateUtils {
    /**
     * 时间格式(yyyy-MM-dd)
     */
    public final static String DATE_PATTERN = "yyyy-MM-dd";
    /**
     * 时间格式(yyyy-MM-dd HH:mm:ss)
     */
    public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 时间格式(yyyy-MM-dd HH:mm:ss)
     */
    public final static String DATE_TIME_PATTERN_T = "yyyy-MM-dd'T'HH:mm:ss";

    public static String format(Date date) {
        return format(date, DATE_PATTERN);
    }

    public static String format(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            df.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
            return df.format(date);
        }
        return null;
    }

    public static Date dateFormat(String date, String pattern) {
        if (date != null && !"".equals(date)) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            df.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
            try {
                return df.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Date dateFormat(String date) {
        return dateFormat(date, DATE_TIME_PATTERN);
    }

    public static Date dateFormatT(String date) {
        return dateFormat(date, DATE_TIME_PATTERN_T);
    }

    /* 获取前几天的日期
     *
     * @param hour
     */
    public static String getPreHoursStr(int hour) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, -hour);
        Date date = c.getTime();
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 指定日期按指定格式显示
     *
     * @param date    指定的日期
     * @param pattern 指定的格式
     * @return 指定日期按指定格式显示
     */
    public static String formatDate(Date date, String pattern) {
        return getSDFormat(pattern).format(date);
    }
    // 指定模式的时间格式
    private static SimpleDateFormat getSDFormat(String pattern) {
        return new SimpleDateFormat(pattern);
    }

    public static Boolean equalsToday(Date date){
        String s = format(date);
        String today = format(new Date());
        return s.equals(today);
    }

    public static Boolean equalsYesterday(Date date){
        String s = format(date);
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE,-1);
        String yesterday = format(instance.getTime());
        return s.equals(yesterday);
    }

    public static void main(String[] agrs){
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE,-1);
        Boolean b = equalsYesterday(instance.getTime());
//        Boolean b2 = equasToday(new Date());
        System.out.println(b);
    }
}
