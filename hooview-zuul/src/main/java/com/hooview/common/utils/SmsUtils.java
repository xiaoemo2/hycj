package com.hooview.common.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.IClientProfile;
import com.hooview.config.SmsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.aliyuncs.profile.DefaultProfile.addEndpoint;
import static com.aliyuncs.profile.DefaultProfile.getProfile;

public class SmsUtils {
    private static Logger logger = LoggerFactory.getLogger(SmsUtils.class);


    public static SendSmsResponse sendSms(String mobile, String code, String outId) throws ClientException {
        SmsConfig config = SpringContextUtils.getBean("smsConfig", SmsConfig.class);

        String templateParam = "{\"code\":\"" + code + "\"}";
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = getProfile(config.getRegionld(), config.getAccessKeyId(), config.getAccessKeySecret());
        addEndpoint(config.getEndpoint(), config.getRegionld(), config.getProduct(), config.getDomain());
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(mobile);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(config.getSignName());
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(config.getTemplVerifyCode());
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(templateParam);

        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");

        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        //request.setOutId("yourOutId");
        request.setOutId(outId);

        //hint 此处可能会抛出异常，注意catch
        logger.info("--------调用阿里云发送短信--------");
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
        logger.info("----------阿里云短信发送成功--------------");
        return sendSmsResponse;
    }
}
