package com.hooview.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 对应jqgrid查询条件
 */
public class QueryFilters {
    //    groupOps: [ { op: "AND", text: "所有" },    { op: "OR",  text: "任一" } ],
    private String groupOp = "AND";
    private String sidx;
    private String sord = "ASC";
    private List<QueryRule> rules = new ArrayList<>();
    private Integer pageNo = 1;
    private Integer pageSize = 10;

    public String getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(String groupOp) {
        this.groupOp = groupOp;
    }

    public List<QueryRule> getRules() {
        return rules;
    }

    public void setRules(List<QueryRule> rules) {
        this.rules = rules;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
