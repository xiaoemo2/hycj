package com.hooview.utils;

/**
 * 核心服务枚举
 *
 * @author gavincook
 * @version $ID: BaseEnum.java, v0.1 2018-01-03 19:19 gavincook Exp $$
 */
public enum CoreEnum {

    /** 成功 */
    SUCCESS("SUCCESS", "成功"),

    /** 未登录 */
    NOT_LOGIN("NOT_LOGIN", "未登录"),

    /** 数据库异常 */
    DATABASE_EXCEPTION("DATABASE_EXCEPTION", "数据库异常"),

    /** 未知异常 */
    UNKNOWN_EXCEPTION("UNKNOWN_EXCEPTION", "未知异常"),

    /** 系统异常 */
    SYSTEM_ERROR("SYSTEM_ERROR", "系统异常"),

    /** 空值对象 */
    NULL_OBJECT("NULL_OBJECT", "对象为NULL"),

    ASSERT_NOT_EQUAL("ASSERT_NOT_EQUAL", "实际值和预期值需要不相等，实际值：%s"),

    ASSERT_EQUAL("ASSERT_EQUAL", "需要值：%s，实际值：%s"),

    ASSERT_TRUE("ASSERT_TRUE", "条件需要为true"),

    ASSERT_FALSE("ASSERT_FALSE", "条件需要为false"),

    /** 数字不为正数 */
    ASSERT_NOT_POSITIVE("ASSERT_NOT_POSITIVE", "对象需要为正数"),

    /** 数字不为正整数 */
    ASSERT_NOT_POSITIVE_INTEGER("ASSERT_NOT_POSITIVE_INTEGER", "对象需要为正整数"),

    /** 数字不为大于等于0的整数 */
    ASSERT_NOT_INTEGER_GE_ZERO("ASSERT_NOT_INTEGER_GE_ZERO", "对象需要为大于等于0的整数"),

    /** 空对象 */
    EMPTY_OBJECT("EMPTY_OBJECT", "空对象"),

    OVER_MAX_SIZE("OVER_MAX_SIZE", "超过最大值"),

    PAGE_CONDITION_INVALID("PAGE_CONDITION_INVALID", "分页数据异常"),

    FILE_TYPE_NOT_FOUND("FILE_TYPE_NOT_FOUND", "没有找到对应的文件类型"),

    /** 默认类型，用于断言的业务异常使用 */
    DEFAULT("DEFAULT", "默认");

    /** 枚举值 */
    private String code;

    /** 枚举描述 */
    private String desc;

    /**
     * 构造方法
     *
     * @param code
     * @param desc
     */
    private CoreEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
