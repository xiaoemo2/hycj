package com.hooview.utils;

/**
 * sql条件包装器
 *
 * @author gavincook
 * @version $ID: SqlConditionWrapper.java, v0.1 2018-01-18 14:13 gavincook Exp $$
 */
public class SqlConditionWrapper {

    /**
     * 模糊匹配
     *
     * @param value
     * @return
     */
    public static String wildcard(String value) {
        if (value == null) {
            return null;
        }
        return "%" + value + "%";
    }

}
