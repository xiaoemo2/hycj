package com.hooview.web.template;

/**
 * 模板回调类
 *
 * @author gavincook
 * @version $Id: ExecuteCallBack.java, v0.1 2018-01-03 19:26 gavincook Exp $$
 */
public interface ExecuteCallBack {

    /**
     * 相关检查
     */
    default void check() {
    }


    /**
     * 调用服务
     */
    void service();

}
