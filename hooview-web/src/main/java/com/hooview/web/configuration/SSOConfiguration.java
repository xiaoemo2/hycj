package com.hooview.web.configuration;

import me.gavincook.sso.client.interceptor.SSOInterceptor;
import me.gavincook.sso.client.interceptor.spring.ServletSSOFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * SSO集成配置
 *
 * @author gavincook
 * @version $ID: SSOConfiguration.java, v0.1 2018-05-29 09:15 gavincook Exp $$
 */
@Configuration
@ConfigurationProperties(prefix = "sso")
public class SSOConfiguration {

    /** 登录页面地址 */
    private String loginUrl;

    /** api地址 */
    private String apiUrl;

    /** 应用名称 */
    private String appName;

    /** sso开关 */
    private boolean flag;

    /**
     * 创建sso拦截器，并打开sso开关
     *
     * @return
     */
    @Bean
    public SSOInterceptor ssoInterceptor() {
        SSOInterceptor ssoInterceptor = new SSOInterceptor(loginUrl, apiUrl, appName);
        ssoInterceptor.setInterceptorOn(flag);
        return ssoInterceptor;
    }

    /**
     * sso servlet过滤器
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean ssoFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new ServletSSOFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setName("ssoFilter");
        return filterRegistrationBean;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
