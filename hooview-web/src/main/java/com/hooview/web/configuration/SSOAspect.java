/**
 * BBD Service Inc
 * All Rights Reserved @2018
 */
package com.hooview.web.configuration;

import me.gavincook.sso.client.interceptor.SSOInterceptor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * SSOAspect
 *
 * @author gavincook
 * @version $ID: SSOAspect.java, v0.1 2018-05-29 11:53 gavincook Exp $$
 */
@Aspect
@Component
public class SSOAspect {

    @Autowired
    private SSOInterceptor ssoInterceptor;

    /**
     * 拦截所有的controlle
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("execution(* com.hooview.web.controller..*.*(..))")
    public Object ssoIntercept(ProceedingJoinPoint joinPoint) throws Throwable {
        return ssoInterceptor.doAround(joinPoint);
    }

}
