package com.hooview.web.controller;

import com.hooview.api.entity.Notices;
import com.hooview.api.service.JPushService;
import com.hooview.api.service.NoticeService;
import com.hooview.utils.AjaxMsg;
import com.hooview.utils.AssertUtils;
import com.hooview.web.template.ExecuteCallBack;
import com.hooview.web.template.ExecuteTemplate;
import me.gavincook.sso.client.annotation.LoginRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * PushController
 *
 * @author gavincook
 * @version $ID: PushController.java, v0.1 2018-08-11 17:48 gavincook Exp $$
 */
@RestController
@RequestMapping("/push")
public class PushController {

    @Autowired
    private ExecuteTemplate executeTemplate;

    @Autowired
    private JPushService pushService;

    @Autowired
    private NoticeService noticeService;

    /**
     * 新建推送
     *
     * @param title 推送标题
     * @param content 推送内容
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    @LoginRequired
    public AjaxMsg push(@RequestParam("title") String title, @RequestParam("content") String content) {
        AjaxMsg ajaxMsg = new AjaxMsg();

        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(title, "推送标题不能为空");
                AssertUtils.assertNotNull(content, "推送内容不能为空");
            }

            @Override
            public void service() {
                pushService.push(title, content);
                Notices notices = new Notices();
                notices.setContent(content);
                notices.setTitle(title);
                notices.setType("00");
                noticeService.insert(notices);
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

}
