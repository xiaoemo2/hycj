package com.hooview.web.controller;

import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.DictionaryService;
import com.hooview.utils.*;
import com.hooview.web.template.ExecuteCallBack;
import com.hooview.web.template.ExecuteTemplate;
import me.gavincook.sso.client.annotation.LoginRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统一些业务配置资源处理器
 * @author Hinsteny
 * @version $ID: ManagementController.java, v0.1 2018-05-26 22:58 Hinsteny Exp $$
 */
@RestController
@RequestMapping("/managements")
@LoginRequired
public class ManagementController {

    @Autowired
    private ExecuteTemplate executeTemplate;

    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 分享底图图片的字典存储KEY
     */
    private static final String SHARE_IMAGE_BG_KEY = "SHARE_BGIMAGE";

    /**
     * 查询分享底图的图片地址
     *
     * @return
     */
    @RequestMapping(value = "/share-bgimages", method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg queryShareBGIMages() {
        AjaxMsg ajaxMsg = new AjaxMsg();
        QueryFilters filters = new QueryFilters();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                QueryRule queryRule = new QueryRule();
                queryRule.setField("key_no");
                queryRule.setOp("like");
                queryRule.setData(SqlConditionWrapper.wildcard(SHARE_IMAGE_BG_KEY));
                filters.getRules().add(queryRule);
            }

            @Override
            public void service() {
                // 查询服务
                List<Dictionary> list = dictionaryService.getByFilter(filters);
                Dictionary result = null;
                if (null != list && list.size() > 0) {
                    result = list.get(0);
                }
                ajaxMsg.setDatas(result);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 更新分享底图的图片地址
     *
     * @param dictionary 字典实体
     * @return
     */
    @RequestMapping(value = "/share-bgimages", method = RequestMethod.PUT)
    @ResponseBody
    public AjaxMsg updateShareBGIMages(@RequestBody Dictionary dictionary) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(dictionary);
//                AssertUtils.assertPositiveIntegerNumber(dictionary.getId());
                dictionary.setKeyNo(SHARE_IMAGE_BG_KEY);
                AssertUtils.assertNotNull(dictionary.getKeyValue(), "分享底图存储字典VALUE不能为空");
            }

            @Override
            public void service() {
                ajaxMsg.setDatas(dictionaryService.update(dictionary) > 0);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

}
