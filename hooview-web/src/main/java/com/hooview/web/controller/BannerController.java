package com.hooview.web.controller;

import com.hooview.api.entity.Banner;
import com.hooview.api.service.BannerService;
import com.hooview.utils.AjaxMsg;
import com.hooview.utils.AssertUtils;
import com.hooview.utils.Paginator;
import com.hooview.web.template.ExecuteCallBack;
import com.hooview.web.template.ExecuteTemplate;
import me.gavincook.sso.client.annotation.LoginRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 轮播图控制器
 *
 * @author gavincook
 * @version $ID: BannerController.java, v0.1 2018-06-10 12:30 gavincook Exp $$
 */
@RestController
@RequestMapping("/banners")
public class BannerController {

    @Autowired
    private ExecuteTemplate executeTemplate;

    @Autowired
    private BannerService bannerService;

    /**
     * 轮播搜索
     *
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @LoginRequired
    public AjaxMsg search() {

        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(() -> {
            //分页数据
            List<Banner> list = bannerService.queryList(new HashMap<>());

            //构建分页对象
            Paginator<Banner> paginator = new Paginator<>(list.size(), list, 1, list.size());
            ajaxMsg.setDatas(paginator);

        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 新增轮播
     *
     * @param banner
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    @LoginRequired
    public AjaxMsg createBanner(@RequestBody Banner banner) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(banner);
                AssertUtils.assertPositiveIntegerNumber(banner.getSort(), "显示顺序需大于0");
            }

            @Override
            public void service() {
                banner.setCreateTime(new Date());
                banner.setCreateUserId(-1);
                bannerService.save(banner);
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 更新轮播
     *
     * @param banner
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    @LoginRequired
    public AjaxMsg updateBanner(@RequestBody Banner banner) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(banner);
            }

            @Override
            public void service() {
                bannerService.update(banner);
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }


    /**
     * 删除轮播
     *
     * @param banner
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ResponseBody
    @LoginRequired
    public AjaxMsg deleteBanner(@RequestBody Banner banner) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(banner);
                AssertUtils.assertPositiveIntegerNumber(banner.getId(), "ID不能为空");
            }

            @Override
            public void service() {
                bannerService.delete(banner.getId());
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

}
