package com.hooview.web.controller;

import com.github.pagehelper.PageHelper;
import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.DictionaryService;
import com.hooview.utils.*;
import com.hooview.web.template.ExecuteCallBack;
import com.hooview.web.template.ExecuteTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * app地址管理器
 *
 * @author Divers King
 * @date 2018-05-28
 **/
@RestController
@RequestMapping("/app-addresses")
public class AppAddressController {

    @Autowired
    private ExecuteTemplate executeTemplate;

    @Autowired
    private DictionaryService dictionaryService;

    /**
     * app地址查询
     *
     * @param pageSize  每页条数
     * @param pageNum   页码
     * @return ajaxMsg
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public AjaxMsg search(Integer pageSize, Integer pageNum) {

        QueryFilters filters = new QueryFilters();

        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                PageHelper.startPage(pageNum, pageSize);

                QueryRule queryRule = new QueryRule();
                queryRule.setField("key_no");
                queryRule.setOp("like");
                queryRule.setData(SqlConditionWrapper.wildcard("APP_ADDRESS"));
                filters.getRules().add(queryRule);
            }

            @Override
            public void service() {
                //分页数据
                List<Dictionary> list = dictionaryService.getByFilter(filters);

                //如果计数器为0，则表示没有数据
                if (list.size() == 0) {
                    ajaxMsg.setDatas(Paginator.ofEmpty(pageSize));
                    return;
                }

                //构建分页对象
                Paginator<Dictionary> paginator = new Paginator<>(list.size(), list, pageNum, pageSize);
                ajaxMsg.setDatas(paginator);

            }
        }, ajaxMsg);

        return ajaxMsg;
    }

    /**
     * app地址修改
     * @param dictionary
     *      {
     *          id: xx,
     *          keyNo: xxx, //key
     *          keyValue: xxx, //app地址
     *          caption：xxx //其他说明
     *      }
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public AjaxMsg createNews(@RequestBody Dictionary dictionary) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(dictionary);
                AssertUtils.assertPositiveIntegerNumber(dictionary.getId());
                AssertUtils.assertNotNull(dictionary.getKeyNo(), "APP地址key不能为空");
                AssertUtils.assertNotNull(dictionary.getKeyValue(), "APP地址不能为空");
            }

            @Override
            public void service() {
                ajaxMsg.setDatas(dictionaryService.update(dictionary) > 0);
            }
        }, ajaxMsg);

        return ajaxMsg;
    }

}
