package com.hooview.web.controller;

import com.hooview.api.entity.SignRule;
import com.hooview.api.service.SignRuleService;
import com.hooview.utils.AjaxMsg;
import com.hooview.utils.AssertUtils;
import com.hooview.utils.Paginator;
import com.hooview.web.template.ExecuteCallBack;
import com.hooview.web.template.ExecuteTemplate;
import me.gavincook.sso.client.annotation.LoginRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 签到规则控制器
 *
 * @author gavincook
 * @version $ID: SignRuleController.java, v0.1 2018-06-09 23:58 gavincook Exp $$
 */
@RestController
@RequestMapping("/sign-rules")
public class SignRuleController {

    @Autowired
    private ExecuteTemplate executeTemplate;

    @Autowired
    private SignRuleService signRuleService;

    /**
     * 签到规则搜索
     *
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @LoginRequired
    public AjaxMsg search() {

        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(() -> {
            //分页数据
            List<SignRule> list = signRuleService.findAll();

            //构建分页对象
            Paginator<SignRule> paginator = new Paginator<>(list.size(), list, 1, list.size());
            ajaxMsg.setDatas(paginator);

        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 新增签到规则
     *
     * @param signRule 签到规则实体
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    @LoginRequired
    public AjaxMsg createSignRule(@RequestBody SignRule signRule) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(signRule);
                AssertUtils.assertPositiveIntegerNumber(signRule.getRewardCoins(), "糖果数应该大于0");
                AssertUtils.assertPositiveIntegerNumber(signRule.getSignDays(), "连续签到天数应该大于0");
            }

            @Override
            public void service() {
                signRule.setCreateTime(new Date());
                signRule.setUpdateTime(new Date());
                signRuleService.save(signRule);
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 更新签到规则
     *
     * @param signRule 签到规则实体
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    @LoginRequired
    public AjaxMsg updateSignRule(@RequestBody SignRule signRule) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(signRule);
            }

            @Override
            public void service() {
                signRuleService.update(signRule);
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }


    /**
     * 删除签到规则
     *
     * @param signRule 签到规则实体
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ResponseBody
    @LoginRequired
    public AjaxMsg deleteSignRule(@RequestBody SignRule signRule) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(signRule);
                AssertUtils.assertStringNotBlank(signRule.getId(), "ID不能为空");
            }

            @Override
            public void service() {
                signRuleService.delete(signRule.getId());
                ajaxMsg.setDatas(true);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 根据签到天数检测是否有对应规则
     *
     * @param signDays 签到天数
     * @return
     */
    @RequestMapping(value = "/get-by-sign-days", method = RequestMethod.GET)
    @ResponseBody
    @LoginRequired
    public AjaxMsg checkExist(Integer signDays) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertPositiveIntegerNumber(signDays, "签到天数需为大于零的整数");
            }

            @Override
            public void service() {
                ajaxMsg.setDatas(signRuleService.findBySigndays(signDays) != null);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }
}
