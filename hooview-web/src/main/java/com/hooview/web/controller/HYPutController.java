package com.hooview.web.controller;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyInput;
import com.hooview.api.service.HyInputService;
import com.hooview.utils.AjaxMsg;
import com.hooview.utils.AssertUtils;
import com.hooview.utils.Paginator;
import com.hooview.web.template.ExecuteCallBack;
import com.hooview.web.template.ExecuteTemplate;
import me.gavincook.sso.client.annotation.LoginRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * HY投放管理
 *
 * @author gavincook
 * @version $ID: HYPutController.java, v0.1 2018-08-08 10:51 gavincook Exp $$
 */
@RestController
@RequestMapping("/hy-puts")
@LoginRequired
public class HYPutController {

    @Autowired
    private ExecuteTemplate executeTemplate;

    @Autowired
    private HyInputService hyInputService;

    /**
     * HY投放搜索
     *
     * @param pageSize 每页条数
     * @param pageNum  页码
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @LoginRequired
    public AjaxMsg search(Integer pageSize, Integer pageNum) {

        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(() -> {
            //分页数据
            Page<HyInput> list = hyInputService.findAll(pageNum, pageSize);

            //构建分页对象
            Paginator<HyInput> paginator = new Paginator<>(list.getTotal(), list, list.getPageNum(), list.getPageSize());
            ajaxMsg.setDatas(paginator);

        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 新增HY投放
     *
     * @param hyInput  HY实体
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    @LoginRequired
    public AjaxMsg createHyInput(@RequestBody HyInput hyInput) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(hyInput);
                AssertUtils.assertPositiveIntegerNumber(hyInput.getHy(), "投放量应该大于0");
                AssertUtils.assertPositiveIntegerNumber(hyInput.getRadix(), "基数应该大于0");
                LocalDateTime currentDay = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0, 0));
                long currentDayMilli = currentDay.toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
                AssertUtils.assertTrue(currentDayMilli <= hyInput.getCreateTime().getTime(), "投放时间应该为今天及以后");
            }

            @Override
            public void service() {
                hyInput.setUpdateTime(new Date());
                ajaxMsg.setDatas(hyInputService.insert(hyInput));
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 更新HY投放
     *
     * @param hyInput HY实体
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    @LoginRequired
    public AjaxMsg updateHyInput(@RequestBody HyInput hyInput) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(hyInput);
            }

            @Override
            public void service() {
                ajaxMsg.setDatas(hyInputService.update(hyInput));
            }
        }, ajaxMsg);


        return ajaxMsg;
    }


    /**
     * 删除HY投放
     *
     * @param hyInput HY实体
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.DELETE)
    @ResponseBody
    @LoginRequired
    public AjaxMsg deleteHyInput(@RequestBody HyInput hyInput) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
                AssertUtils.assertNotNull(hyInput);
                AssertUtils.assertNotNull(hyInput.getId(), "ID不能为空");
            }

            @Override
            public void service() {
                ajaxMsg.setDatas(hyInputService.delete(hyInput.getId()));
            }
        }, ajaxMsg);


        return ajaxMsg;
    }

    /**
     * 根据时间检测是否有对应投放
     *
     * @param createTime 创建时间
     * @return
     */
    @RequestMapping(value = "/get-by-create-time", method = RequestMethod.GET)
    @ResponseBody
    @LoginRequired
    public AjaxMsg checkExist(Date createTime) {
        AjaxMsg ajaxMsg = new AjaxMsg();
        executeTemplate.executeWithoutTransaction(new ExecuteCallBack() {
            @Override
            public void check() {
            }

            @Override
            public void service() {
                ajaxMsg.setDatas(false);
            }
        }, ajaxMsg);


        return ajaxMsg;
    }
}
