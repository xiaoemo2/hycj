package com.hooview.web.vo;

import com.hooview.api.entity.News;

/**
 * NewsVO
 *
 * @author gavincook
 * @version $ID: NewsVO.java, v0.1 2018-06-09 16:00 gavincook Exp $$
 */
public class NewsVO extends News {

    /** 过期时间戳 */
    private Long expireTimeStamp;

    public Long getExpireTimeStamp() {
        return expireTimeStamp;
    }

    public void setExpireTimeStamp(Long expireTimeStamp) {
        this.expireTimeStamp = expireTimeStamp;
    }
}
