package com.hooview.api.service.fallback;

import com.hooview.api.entity.User;
import com.hooview.api.service.UserService;

import java.util.Map;

/**
 * UserServiceFallBack
 *
 * @author gavincook
 * @version $ID: UserServiceFallBack.java, v0.1 2018-06-12 22:05 gavincook Exp $$
 */
public class UserServiceFallBack implements UserService {

    @Override
    public Map queryByParams(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public Map queryMediaUserByParams(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public void update(User user) {

    }
}
