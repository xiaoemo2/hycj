package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyInput;
import com.hooview.api.service.fallback.HyInputServiceFallBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "hooview-service", fallback = HyInputServiceFallBack.class)
public interface HyInputService {
    /**
     * 投放 createTime 字段为使用时间
     * 使用时间不存在则 成功 true 否则 失败false
     *
     * @param hyInput
     */
    @RequestMapping(value = "/hyInput/insert", method = RequestMethod.POST)
    Boolean insert(@RequestBody HyInput hyInput);

    /**
     * 根据id删除
     * 使用时间在当前时间后删除成功返回true 否则失败 false
     *
     * @param id
     */
    @RequestMapping(value = "/hyInput/delete", method = RequestMethod.GET)
    Boolean delete(@RequestParam("id") Integer id);

    /**
     * 更新
     * 使用时间在当前时间后成功 true 否则失败 false
     *
     * @param hyInput
     */
    @RequestMapping(value = "/hyInput/update", method = RequestMethod.POST)
    Boolean update(@RequestBody HyInput hyInput);

    /**
     * 根据 id查询
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/hyInput/findById", method = RequestMethod.GET)
    HyInput findById(@RequestParam("id") Integer id);

    /**
     * 投放记录列表 前七天 后面所有
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/hyInput/findAll", method = RequestMethod.GET)
    Page<HyInput> findAll(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);
}
