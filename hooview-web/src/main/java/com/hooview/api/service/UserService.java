package com.hooview.api.service;

import com.hooview.api.entity.User;
import com.hooview.api.service.fallback.UserServiceFallBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(value = "hooview-service", fallback = UserServiceFallBack.class)
public interface UserService {
    /**
     * 运营后台查询
     */
    @RequestMapping(value = "/api/user/queryByParams", method = RequestMethod.POST)
    Map queryByParams(@RequestBody Map<String, Object> params, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);

    @RequestMapping(value = "/api/user/queryMediaUser", method = RequestMethod.POST)
    Map queryMediaUserByParams(@RequestBody Map<String, Object> params, @RequestParam(name = "pageNum") Integer pageNum, @RequestParam(name = "pageSize") Integer pageSize);

    @RequestMapping(value = "/api/user/update", method = RequestMethod.POST)
    void update(@RequestBody User user);
}
