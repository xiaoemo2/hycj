package com.hooview.api.service;

import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.fallback.DictionaryServiceFallBack;
import com.hooview.utils.QueryFilters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author Divers King
 * @date 2018-05-28
 **/
@FeignClient(value = "hooview-service", fallback = DictionaryServiceFallBack.class)
public interface DictionaryService {

    /**
     * 根据条件查询
     * @param filters 属性列表，过滤存在的属性值
     */
    @RequestMapping(value = "/dictionary/getByFilter", method = RequestMethod.POST)
    List<Dictionary> getByFilter(@RequestBody QueryFilters filters);

    /**
     * 修改
     */
    @RequestMapping(value = "/dictionary/update", method = RequestMethod.POST)
    Integer update(@RequestBody Dictionary entity);

    /**
     * 添加
     */
    @RequestMapping(value = "/dictionary/insert", method = RequestMethod.POST)
    Dictionary insert(@RequestBody Dictionary entity);

    /**
     * 删除
     */
    @RequestMapping(value = "/dictionary/delete", method = RequestMethod.GET)
    void delete(@RequestParam("id") Integer id);

    /**
     * 根据keyNo获取字典信息
     */
    @RequestMapping(value = "/dictionary/getDicByKeyNo", method = RequestMethod.GET)
    List<Dictionary> getDicByKeyNo(@RequestParam("keyNo") String keyNo);
}
