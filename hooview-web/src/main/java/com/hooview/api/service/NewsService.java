package com.hooview.api.service;

import com.hooview.api.entity.News;
import com.hooview.api.service.fallback.NewsServiceFallBack;
import com.hooview.utils.QueryFilters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(value = "hooview-service", fallback = NewsServiceFallBack.class)
public interface NewsService {
    @RequestMapping(value = "/news/delete", method = RequestMethod.POST)
    int deleteByPrimaryKey(@RequestParam("id") Integer id);

    @RequestMapping(value = "/news/save", method = RequestMethod.POST)
    int insert(@RequestBody News record);

    @RequestMapping(value = "/news/queryObject", method = RequestMethod.GET)
    News selectByPrimaryKey(@RequestParam("id") Integer id);

    @RequestMapping(value = "/news/update", method = RequestMethod.POST)
    int updateByPrimaryKeySelective(@RequestBody News record);

    @RequestMapping(value = "/news/queryList", method = RequestMethod.POST)
    List<News> queryByParams(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/news/getByFilter", method = RequestMethod.POST)
    List<News> getByFilter(@RequestBody QueryFilters filters)
            ;
    @RequestMapping(value = "/news/getMapByFilter", method = RequestMethod.POST)
    List<Map<String,Object>> getMapByFilter(@RequestBody QueryFilters filters);

    @RequestMapping(value = "/news/getCountByFilter", method = RequestMethod.POST)
    Integer getCountByFilter(@RequestBody QueryFilters filters);
}