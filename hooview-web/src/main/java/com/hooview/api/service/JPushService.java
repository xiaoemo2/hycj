/**
 * BBD Service Inc
 * All Rights Reserved @2018
 */
package com.hooview.api.service;

/**
 * JPushService
 *
 * @author gavincook
 * @version $ID: JPushService.java, v0.1 2018-06-10 09:58 gavincook Exp $$
 */
public interface JPushService {

    void pushNews(Integer id);

    void push(String title, String content);
}
