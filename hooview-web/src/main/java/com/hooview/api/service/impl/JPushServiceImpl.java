package com.hooview.api.service.impl;

import com.hooview.api.entity.News;
import com.hooview.api.service.JPushService;
import com.hooview.api.service.NewsService;
import com.hooview.utils.JpushClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * JPushServiceImpl
 *
 * @author gavincook
 * @version $ID: JPushServiceImpl.java, v0.1 2018-06-10 10:24 gavincook Exp $$
 */
@Component
public class JPushServiceImpl implements JPushService {
    @Autowired
    private NewsService newsService;

    @Override
    public void pushNews(Integer id) {
        News news = newsService.selectByPrimaryKey(id);
        //快讯
        if (news.getChannelId() == 7)
            JpushClient.sendPush(news.getTitle(), news.getContent(), 0, null);
        //微博
        if (news.getChannelId() == 2)
            JpushClient.sendPush(news.getTitle(), news.getContent(), 1, null);
    }

    @Override
    public void push(String title, String content) {
        JpushClient.sendPush(title, content, 0, null);
    }
}
