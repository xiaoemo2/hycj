package com.hooview.api.service.fallback;

import com.hooview.api.entity.News;
import com.hooview.api.service.NewsService;
import com.hooview.utils.QueryFilters;

import java.util.List;
import java.util.Map;

/**
 * 资讯回退服务
 *
 * @author gavincook
 * @version $ID: NewsServiceFallBack.java, v0.1 2018-05-25 11:53 gavincook Exp $$
 */
public class NewsServiceFallBack implements NewsService {
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(News record) {
        return 0;
    }

    @Override
    public News selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(News record) {
        return 0;
    }

    @Override
    public List<News> queryByParams(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<News> getByFilter(QueryFilters filters) {
        return null;
    }
    @Override
    public List<Map<String,Object>> getMapByFilter(QueryFilters filters) {
        return null;
    }

    @Override
    public Integer getCountByFilter(QueryFilters filters) {
        return null;
    }
}
