package com.hooview.api.service;

import com.hooview.api.entity.SignRule;
import com.hooview.api.service.fallback.SignRuleServiceFallBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "hooview-service", fallback = SignRuleServiceFallBack.class)
public interface SignRuleService {

    @RequestMapping(value = "/api/signRule/findAll", method = RequestMethod.GET)
    List<SignRule> findAll();


    @RequestMapping(value = "/api/signRule/save", method = RequestMethod.POST)
    void save(@RequestBody SignRule signRule);

    @RequestMapping(value = "/api/signRule/update", method = RequestMethod.POST)
    void update(@RequestBody SignRule signRule);

    @RequestMapping(value = "/api/signRule/delete", method = RequestMethod.GET)
    void delete(@RequestParam("id") String id);

    @RequestMapping(value = "/api/signRule/findById", method = RequestMethod.GET)
    SignRule findById(@RequestParam("id") String id);

    @RequestMapping(value = "/api/signRule/findBySigndays", method = RequestMethod.GET)
    SignRule findBySigndays(@RequestParam("signDays") int signDays);
}
