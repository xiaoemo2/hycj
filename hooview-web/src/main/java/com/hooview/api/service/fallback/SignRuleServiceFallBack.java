package com.hooview.api.service.fallback;

import com.hooview.api.entity.SignRule;
import com.hooview.api.service.SignRuleService;

import java.util.List;

/**
 * SignRuleServiceFallBack
 *
 * @author gavincook
 * @version $ID: SignRuleServiceFallBack.java, v0.1 2018-06-09 23:55 gavincook Exp $$
 */
public class SignRuleServiceFallBack implements SignRuleService {
    @Override
    public List<SignRule> findAll() {
        return null;
    }


    @Override
    public void save(SignRule signRule) {

    }

    @Override
    public void update(SignRule signRule) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public SignRule findById(String id) {
        return null;
    }

    @Override
    public SignRule findBySigndays(int signDays) {
        return null;
    }
}
