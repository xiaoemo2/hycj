package com.hooview.api.service.fallback;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Notices;
import com.hooview.api.service.NoticeService;

/**
 * NoticeServiceFallBack
 *
 * @author gavincook
 * @version $ID: NoticeServiceFallBack.java, v0.1 2018-08-11 23:30 gavincook Exp $$
 */
public class NoticeServiceFallBack implements NoticeService {
    @Override
    public Page<Notices> findByType(String type, Integer pageNum, Integer pageSize) {
        return null;
    }

    @Override
    public void insert(Notices notices) {

    }

    @Override
    public void update(Notices notices) {

    }

    @Override
    public void delete(Integer id) {

    }
}
