package com.hooview.api.service.fallback;

import com.hooview.api.entity.Banner;
import com.hooview.api.service.BannerService;
import com.hooview.utils.QueryFilters;

import java.util.List;
import java.util.Map;

/**
 * BannerServiceFallBack
 *
 * @author gavincook
 * @version $ID: BannerServiceFallBack.java, v0.1 2018-06-10 12:28 gavincook Exp $$
 */
public class BannerServiceFallBack implements BannerService {
    @Override
    public Banner queryObject(Integer id) {
        return null;
    }

    @Override
    public List<Banner> queryList(Map<String, Object> map) {
        return null;
    }

    @Override
    public void save(Banner banner) {

    }

    @Override
    public void update(Banner banner) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<Banner> getByFilter(QueryFilters filters) {
        return null;
    }
}
