package com.hooview.api.service.fallback;

import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.DictionaryService;
import com.hooview.utils.QueryFilters;

import java.util.List;

/**
 * @author Divers King
 * @date 2018-05-28
 **/
public class DictionaryServiceFallBack implements DictionaryService {

    @Override
    public List<Dictionary> getByFilter(QueryFilters filters) {
        return null;
    }

    @Override
    public List<Dictionary> getDicByKeyNo(String keyNo) {
        return null;
    }

    @Override
    public Integer update(Dictionary entity) {
        return 0;
    }

    @Override
    public Dictionary insert(Dictionary entity) {
        return null;
    }

    @Override
    public void delete(Integer id) {
    }
}
