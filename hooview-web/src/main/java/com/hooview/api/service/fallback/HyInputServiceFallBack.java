package com.hooview.api.service.fallback;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyInput;
import com.hooview.api.service.HyInputService;

/**
 * HyIputServiceFallBack
 *
 * @author gavincook
 * @version $ID: HyIputServiceFallBack.java, v0.1 2018-08-08 10:49 gavincook Exp $$
 */
public class HyInputServiceFallBack implements HyInputService {


    @Override
    public Boolean insert(HyInput hyInput) {
        return null;
    }

    @Override
    public Boolean delete(Integer id) {
        return null;
    }

    @Override
    public Boolean update(HyInput hyInput) {
        return null;
    }

    @Override
    public HyInput findById(Integer id) {
        return null;
    }

    @Override
    public Page<HyInput> findAll(Integer pageNum, Integer pageSize) {
        return null;
    }
}
