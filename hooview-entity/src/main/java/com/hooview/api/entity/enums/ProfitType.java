package com.hooview.api.entity.enums;

public enum ProfitType {
    REGISTER("注册奖励","01"),
    SIGN("签到成功","02"),
    SHARE("分享快讯成功","03"),
    REGISTER_1("一级好友注册成功","04"),
    REGISTER_2("二级好友注册成功","05"),
    SIGN_1("一级好友签到成功","06"),
    SIGN_2("二级好友签到成功","07"),
    SHARE_1("一级好友分享快讯成功","08"),
    SHARE_2("二级好友分享快讯成功","09"),
    SHARE_ARTICLE("分享文章成功","10"),
    SHARE_ARTICLE_1("一级好友分享文章成功","11"),
    SHARE_ARTICLE_2("二级好友分享文章成功","12"),
    READ("阅读文章成功","13"),
    READ_1("一级好友阅读文章成功","14"),
    READ_2("二级好友阅读文章成功","15");

    private String type;
    private String value;

    ProfitType(String type, String value){
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
