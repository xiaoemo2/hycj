package com.hooview.api.entity;

import java.util.Date;

public class Level {
    private String id;

    private Date createTime;

    private Date updateTime;

    private String name;

    private Integer leastNeed;

    private Integer maxNeed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getLeastNeed() {
        return leastNeed;
    }

    public void setLeastNeed(Integer leastNeed) {
        this.leastNeed = leastNeed;
    }

    public Integer getMaxNeed() {
        return maxNeed;
    }

    public void setMaxNeed(Integer maxNeed) {
        this.maxNeed = maxNeed;
    }
}