package com.hooview.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class HyInput {
    private Integer id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;//使用时间

    private Date updateTime;

    private Integer hy;

    private Integer radix;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getHy() {
        return hy;
    }

    public void setHy(Integer hy) {
        this.hy = hy;
    }

    public Integer getRadix() {
        return radix;
    }

    public void setRadix(Integer radix) {
        this.radix = radix;
    }
}