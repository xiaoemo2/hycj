package com.hooview.api.entity.dto;

import com.hooview.api.entity.User;

import java.util.List;
import java.util.Map;

public class AuthorSimpleInfo {
    private String id;
    private String displayName;
    private String avatar;
    private String content;
    private Integer viewCount;
    private Integer newsCount;
    private Integer fansCount;
    private String isFollow;
    private List<Map<String,Object>> news;

    public AuthorSimpleInfo() {
    }

    public AuthorSimpleInfo(User user){
        this.id = user.getId();
        this.displayName = user.getDisplayName();
        this.avatar = user.getAvatar();
        this.content = user.getContent();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getNewsCount() {
        return newsCount;
    }

    public void setNewsCount(Integer newsCount) {
        this.newsCount = newsCount;
    }

    public Integer getFansCount() {
        return fansCount;
    }

    public void setFansCount(Integer fansCount) {
        this.fansCount = fansCount;
    }

    public String getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(String isFollow) {
        this.isFollow = isFollow;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<Map<String, Object>> getNews() {
        return news;
    }

    public void setNews(List<Map<String, Object>> news) {
        this.news = news;
    }
}
