package com.hooview.api.entity;

import java.util.Date;

public class News {
    private Integer id;

    private String title;

    private String tag;

    private Integer channelId;

    private String image;

    private String author;

    private String logo;

    private Integer candy;

    private Integer createUserId;

    private Date createTime;

    private Integer expireTime;

    private String content;

    private String status="01";

    private String emergency="01";

    private String isRecommend = "00";

    private Integer readAmount=0;

    private Integer supportAmount=0;

    private Integer boycottAmount=0;

    public String getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(String isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getCandy() {
        return candy;
    }

    public void setCandy(Integer candy) {
        this.candy = candy;
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Integer expireTime) {
        this.expireTime = expireTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getReadAmount() {
        return readAmount;
    }

    public void setReadAmount(Integer readAmount) {
        this.readAmount = readAmount;
    }

    public Integer getSupportAmount() {
        return supportAmount;
    }

    public void setSupportAmount(Integer supportAmount) {
        this.supportAmount = supportAmount;
    }

    public Integer getBoycottAmount() {
        return boycottAmount;
    }

    public void setBoycottAmount(Integer boycottAmount) {
        this.boycottAmount = boycottAmount;
    }

    public String getEmergency() {
        return emergency;
    }

    public void setEmergency(String emergency) {
        this.emergency = emergency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}