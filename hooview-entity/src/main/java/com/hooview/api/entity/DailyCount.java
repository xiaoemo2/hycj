package com.hooview.api.entity;

import java.util.Date;

public class DailyCount {
    private Integer id;

    private Date createTime = new java.sql.Date(System.currentTimeMillis());

    private Integer readAmount = 0;

    private Integer shareNewsAmount = 0;

    private Integer shareArticleAmount = 0;

    private Integer sign = 0;

    private Integer commentAmount = 0;

    private Integer suppBoycAmount = 0;

    private String userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getReadAmount() {
        return readAmount;
    }

    public void setReadAmount(Integer readAmount) {
        this.readAmount = readAmount;
    }


    public Integer getCommentAmount() {
        return commentAmount;
    }

    public void setCommentAmount(Integer commentAmount) {
        this.commentAmount = commentAmount;
    }

    public Integer getSuppBoycAmount() {
        return suppBoycAmount;
    }

    public void setSuppBoycAmount(Integer suppBoycAmount) {
        this.suppBoycAmount = suppBoycAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getShareNewsAmount() {
        return shareNewsAmount;
    }

    public void setShareNewsAmount(Integer shareNewsAmount) {
        this.shareNewsAmount = shareNewsAmount;
    }

    public Integer getShareArticleAmount() {
        return shareArticleAmount;
    }

    public void setShareArticleAmount(Integer shareArticleAmount) {
        this.shareArticleAmount = shareArticleAmount;
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }
}