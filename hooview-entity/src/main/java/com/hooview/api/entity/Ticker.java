package com.hooview.api.entity;

import java.util.Date;

public class Ticker {
    private Integer id;

    private Integer siteCoinId;

    private String coinCode;
    private String siteName;

    private Date date;

    private Double high;

    private Double vol;

    private Double last;
    private Double lastUsd;

    private Double low;

    private Double buy;

    private Double sell;

    private String unitCode;

    public String getCoinCode() {
        return coinCode;
    }

    public void setCoinCode(String coinCode) {
        this.coinCode = coinCode;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSiteCoinId() {
        return siteCoinId;
    }

    public void setSiteCoinId(Integer siteCoinId) {
        this.siteCoinId = siteCoinId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getVol() {
        return vol;
    }

    public void setVol(Double vol) {
        this.vol = vol;
    }

    public Double getLast() {
        return last;
    }

    public void setLast(Double last) {
        this.last = last;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getBuy() {
        return buy;
    }

    public void setBuy(Double buy) {
        this.buy = buy;
    }

    public Double getSell() {
        return sell;
    }

    public void setSell(Double sell) {
        this.sell = sell;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode == null ? null : unitCode.trim();
    }

    public Double getLastUsd() {
        return lastUsd;
    }

    public void setLastUsd(Double lastUsd) {
        this.lastUsd = lastUsd;
    }
}