package com.hooview.api.entity;

import java.util.Date;

public class Sign {
    private String id;

    private Date createTime = new Date();

    private Date updateTime = new Date();

    private Date lastSigntime;

    private Integer signTimes;

    private String uid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getLastSigntime() {
        return lastSigntime;
    }

    public void setLastSigntime(Date lastSigntime) {
        this.lastSigntime = lastSigntime;
    }

    public Integer getSignTimes() {
        return signTimes;
    }

    public void setSignTimes(Integer signTimes) {
        this.signTimes = signTimes;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }
}