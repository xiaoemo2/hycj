package com.hooview.api.entity;

import java.util.Date;

public class Comment {
    private Long id;

    private Long newsId;

    private Long parentId;

    private String userId;

    private String content;

    private Date createTime=new Date();

    private Integer supportAmount=0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSupportAmount() {
        return supportAmount;
    }

    public void setSupportAmount(Integer supportAmount) {
        this.supportAmount = supportAmount;
    }
}