package com.hooview.api.entity;

public class CurrencyExchange {
    private Integer id;

    private String scur;

    private String tcur;

    private Double rate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScur() {
        return scur;
    }

    public void setScur(String scur) {
        this.scur = scur == null ? null : scur.trim();
    }

    public String getTcur() {
        return tcur;
    }

    public void setTcur(String tcur) {
        this.tcur = tcur == null ? null : tcur.trim();
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}