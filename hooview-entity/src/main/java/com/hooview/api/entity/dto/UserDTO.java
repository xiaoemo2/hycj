package com.hooview.api.entity.dto;

import com.hooview.api.entity.User;

import java.io.Serializable;

public class UserDTO implements Serializable {

    private String id;
    private String mobile;//手机号
    private String openId;//微信
    private String displayName;//昵称
    private String referee;
    private String refereeName;//推荐人昵称
    private String refereeMobile;
    private Double allCandy;//全部糖果
    private Double todayCandy;//今日糖果
    private Integer invitatNum;//邀请人数
    private String media;
    private String mediaType;

    public String getRefereeName() {
        return refereeName;
    }

    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    public String getRefereeMobile() {
        return refereeMobile;
    }

    public void setRefereeMobile(String refereeMobile) {
        this.refereeMobile = refereeMobile;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public UserDTO() {
    }

    public UserDTO(User user) {
        this.displayName = user.getDisplayName();
        this.mobile = user.getUsername();
        this.openId = user.getOpenId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferee() {
        return referee;
    }

    public void setReferee(String refere) {
        this.referee = refere;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Double getAllCandy() {
        return allCandy;
    }

    public void setAllCandy(Double allCandy) {
        this.allCandy = allCandy;
    }

    public Double getTodayCandy() {
        return todayCandy;
    }

    public void setTodayCandy(Double todayCandy) {
        this.todayCandy = todayCandy;
    }

    public Integer getInvitatNum() {
        return invitatNum;
    }

    public void setInvitatNum(Integer invitatNum) {
        this.invitatNum = invitatNum;
    }
}
