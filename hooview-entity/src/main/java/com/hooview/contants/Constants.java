package com.hooview.contants;

public class Constants {
    public static String COIN_STATUS_YES ="01";// 币种启用
    public static String COIN_STATUS_NO ="00";// 币种禁用

    //自媒体账户审核状态：00 未提交，01 审核中， 02已通过，03已拒绝
    public static String USER_MEDIA_STATUS_UNSUBMITTED ="00";
    public static String USER_MEDIA_STATUS_WAIT ="01";
    public static String USER_MEDIA_STATUS_YES ="02";
    public static String USER_MEDIA_STATUS_NO ="03";

    //是否是自媒体账户00：否，01：是
    public static String USER_MEDIA_YES ="01";
    public static String USER_MEDIA_NO ="00";

    public static Integer CHANNEL_01 =1;// 关注
//
    //文章操作 阅读 收藏 取消收藏 关注 取关 利好 利空
    public static final String NEWS_OPREAT_READ ="00";
    public static final String NEWS_OPREAT_COLLECT ="01";
    public static final String NEWS_OPREAT_CANCEL_COLLECT ="02";
    public static final String NEWS_OPREAT_FOLLOW ="03";
    public static final String NEWS_OPREAT_CANCEL_FOLLOW ="04";
    public static final String NEWS_OPREAT_SUPPORT ="05";
    public static final String NEWS_OPREAT_BOYCOTT ="06";


    //点赞 类型
    public static String NEWS_SUPPORT = "00";
    public static String NEWS_BOYCOTT = "01";

    //收益获取类型
    public static final String PROFIT_REGISTER = "00";
    public static final String PROFIT_SIGN = "01";
    public static final String PROFIT_READ = "02";
    public static final String PROFIT_SHARE_NEWS = "03";
    public static final String PROFIT_SHARE_ARTICLE = "04";
    public static final String PROFIT_REFERE_1 = "05";
    public static final String PROFIT_REFERE_2 = "06";
    public static final String PROFIT_SUPPORT = "07";
    public static final String PROFIT_BOYCOTT = "08";

    //阅读文章 每篇收益量
    public static final Double  READ_PROFIT_1 = 5d;
    public static final Double  READ_PROFIT_2 = 3d;
    public static final Double  READ_PROFIT_3 = 2d;
    public static final Double  READ_PROFIT_4_5_6_7_8 = 1d;

    //分享文章 每篇收益量
    public static final Double SHARE_PROFIT_1 = 10D;
    public static final Double SHARE_PROFIT_2 = 8D;
    public static final Double SHARE_PROFIT_3 = 6D;
    public static final Double SHARE_PROFIT_4 = 4D;
    public static final Double SHARE_PROFIT_5 = 2D;

    //利好 利空每次收益量
    public static final Double SUPP_BOYC_PROFIT_1 = 4D;
    public static final Double SUPP_BOYC_PROFIT_2 = 3D;
    public static final Double SUPP_BOYC_PROFIT_3 = 2D;
    public static final Double SUPP_BOYC_PROFIT_4_9 = 1D;

    //收益关联系数
    public static Double REFERE_1 = 0.2;
    public static Double REFERE_2 = 0.1;

    //签到 注册 邀请 奖励收益额
    public static Double SIGN_1 = 5d;
    public static Double SIGN_2 = 10d;
    public static Double SIGN_3 = 15d;
    public static Double SIGN_4 = 20d;
    public static Double SIGN_5 = 25d;
    public static Double SIGN_6 = 30d;
    public static Double SIGN_7 = 35d;
    public static Double REGISTER= 50d;
    public static Double INVITE = 50d;

    //系统消息类型
    public static String SYSYTEM_NOTCIE_ALL ="00";
    public static String SYSYTEM_NOTCIE_MEDIA ="01";
    public static String SYSYTEM_NOTCIE_ORDINARY ="02";
    //文章状态
    public static String NEWS_STATUS_ONLINE ="01";
    public static String NEWS_STATUS_OFFLINE ="00";

    public static String IOS_CHECK ="IOS_CHECK";
    public static String IOS_VERSION ="IOS_VERSION";
}
