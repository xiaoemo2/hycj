package com.hooview.service.impl;

import com.hooview.api.dao.UserMapper;
import com.hooview.api.entity.User;
import com.hooview.dao.SysMenuDao;
import com.hooview.dao.SysUserDao;
import com.hooview.dao.SysUserTokenDao;
import com.hooview.entity.SysUserEntity;
import com.hooview.service.ShiroService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ShiroServiceImpl implements ShiroService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryByToken(String token) {
        return userMapper.queryByToken(token);
    }

}
