package com.hooview.service;

import com.hooview.api.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * shiro相关接口
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-06-06 8:49
 */
@RestController
public interface ShiroService {

    /**
     * 根据token获取用户
     * @param token
     * @return
     */
    @RequestMapping(value = "/user/queryByToken",method = RequestMethod.GET)
    User queryByToken(@RequestParam("token") String token);

}
