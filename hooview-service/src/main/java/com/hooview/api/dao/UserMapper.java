package com.hooview.api.dao;

import com.github.pagehelper.Page;
import com.hooview.api.entity.User;
import com.hooview.api.entity.dto.AuthorSimpleInfo;
import com.hooview.api.entity.dto.UserDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    void save(User user);

    User queryByUsername(String username);

    User queryByToken(String token);

    User queryByOpenId(String openId);

    Page<UserDTO> queryByParams(Map<String, Object> params);

    Integer countInvitat(String id);

    Page<User> queryMediaUserByParams(Map<String, Object> params);

    User queryAuthorInfo(String authorId);

    List<User> findAll();

    Page<User> queryInvitedFriends(@Param("uid") String uid,@Param("type") String type);
}