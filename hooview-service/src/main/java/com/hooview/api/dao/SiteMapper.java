package com.hooview.api.dao;

import com.hooview.api.entity.Site;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SiteMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Site record);

    int insertSelective(Site record);

    Site selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Site record);

    int updateByPrimaryKeyWithBLOBs(Site record);

    int updateByPrimaryKey(Site record);
}