package com.hooview.api.dao;

import com.hooview.api.entity.News;
import com.hooview.api.entity.Ticker;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TickerMapper {
    int insert(Ticker record);

    List<Ticker> queryByParams(Map<String, Object> map);

    List<Ticker> queryTrend(Map<String, Object> map);

    void deleteByParams(Map<String, Object> map);
}