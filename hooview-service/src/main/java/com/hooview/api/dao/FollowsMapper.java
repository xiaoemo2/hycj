package com.hooview.api.dao;

import com.hooview.api.entity.Follows;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface FollowsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Follows record);

    int insertSelective(Follows record);

    Follows selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Follows record);

    int updateByPrimaryKey(Follows record);

    Follows selectByUidAndAutorId(@Param("uid") String uid,@Param("followerId") String followerId);

    Integer countFollowsByAuthor(String authorId);
}