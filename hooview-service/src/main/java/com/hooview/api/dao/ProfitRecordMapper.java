package com.hooview.api.dao;

import com.github.pagehelper.Page;
import com.hooview.api.entity.ProfitRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.ws.rs.PathParam;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Mapper
public interface ProfitRecordMapper {
    int deleteByPrimaryKey(String id);

    int insert(ProfitRecord record);

    int insertSelective(ProfitRecord record);

    ProfitRecord selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ProfitRecord record);

    int updateByPrimaryKey(ProfitRecord record);

    List<ProfitRecord> queryByParams(Map<String, Object> map);

    Page<ProfitRecord> selectByUser(String uid);

    Double sumProfitByuser(String uid);

    Integer countByUser(String uid);

    Float profitToday(@Param("uid") String uid, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

    ProfitRecord findByUserAndNewsId(@Param("uid") String uid,@Param("newsId") Integer newsId);

    Double queryToday(@Param("uid") String uid,@Param("startDate") Date startDate,@Param("endDate") Date endDate);

    ProfitRecord findByUserAndNewsIdAndType(@Param("uid") String uid,@Param("newsId") Integer newsId,@Param("type") String type);

    Float todayToalProfits(String time);
}