package com.hooview.api.dao;

import com.github.pagehelper.Page;
import com.hooview.api.entity.News;
import com.hooview.utils.QueryFilters;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
@Mapper
public interface NewsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKeyWithBLOBs(News record);

    int updateByPrimaryKey(News record);

    List<News> queryByParams(Map<String, Object> map);


    Page<News> getByFilter(QueryFilters filters);

    Page<Map<String,Object>> getMapByFilter(QueryFilters filters);

    Integer getCountByFilter(QueryFilters filters);

    Integer queryNewsCount(Map<String, Object> map);

    Page<News> findByPage(Map<String, Object> params);

    Integer countNewsByAuthor(String authorId);

    Integer sumReadAmountByAuthor(String authorId);

    Map<String, Object> selectMapByPrimaryKey(Map<String, Object> params);

    Page<Map<String, Object>> findMapByPage(Map<String, Object> params);

    Page<Map<String, Object>> findFollowMapByPage(Map<String, Object> params);

    Page<Map<String, Object>> queryHistoriesByPage(Map<String, Object> params);

    Page<Map<String,Object>> queryHistoriesByPageNo(Map<String, Object> params);

    Page<Map<String, Object>> queryCollectionsByPage(Map<String, Object> params);

    Page<Map<String, Object>> queryAuthorByPage(Map<String, Object> params);

    Page<Map<String,Object>> queryRecommendsByPage(Map<String, Object> params);

    List<Map<String,Object>> findTop5ByReadAmount(String author);

    List<Map<String,Object>> findByKeyword(String keyword);

    Map<String,Object> findNextNews(@Param("currentId") Integer currentId,@Param("channelId") Integer channelId);

    List<Map<String,Object>> findRecommendNews(Integer channelId);
}