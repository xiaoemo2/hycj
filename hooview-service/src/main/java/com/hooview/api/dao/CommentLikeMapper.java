package com.hooview.api.dao;

import com.hooview.api.entity.CommentLike;
import com.hooview.api.entity.Likes;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface CommentLikeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CommentLike record);

    int insertSelective(CommentLike record);

    CommentLike selectByPrimaryKey(Long id);

    List<CommentLike> selectByParams(Map params);

    int updateByPrimaryKeySelective(CommentLike record);

    int updateByPrimaryKey(CommentLike record);
}