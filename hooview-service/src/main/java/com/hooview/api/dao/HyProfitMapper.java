package com.hooview.api.dao;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyProfit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

@Mapper
public interface HyProfitMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HyProfit record);

    int insertSelective(HyProfit record);

    HyProfit selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HyProfit record);

    int updateByPrimaryKey(HyProfit record);

    HyProfit selectByTime(@Param("uid") String uid,@Param("time") String time);

    Page<HyProfit> selectByUser(String uid);
}