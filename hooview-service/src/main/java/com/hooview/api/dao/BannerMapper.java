package com.hooview.api.dao;

import com.hooview.api.entity.Banner;
import com.hooview.utils.QueryFilters;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface BannerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Banner record);

    int insertSelective(Banner record);

    Banner selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Banner record);

    int updateByPrimaryKey(Banner record);

    List<Banner> queryByParams(Map<String, Object> map);

    List<Banner> getByFilter(QueryFilters filters);
}