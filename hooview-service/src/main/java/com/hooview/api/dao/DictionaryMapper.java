package com.hooview.api.dao;

import com.hooview.api.entity.Dictionary;
import com.hooview.utils.QueryFilters;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DictionaryMapper {
    /**
     * 根据条件查询
     *
     * @param filters 属性列表，过滤存在的属性值
     */
    List<Dictionary> getByFilter(QueryFilters filters);

    /**
     * 修改
     */
    Integer update(Dictionary entity);

    /**
     * 添加
     */
    Integer insert(Dictionary entity);

    /**
     * 删除
     */
    void delete(Integer id);

    /**
     * 根据keyNo获取字典信息
     */
    List<Dictionary> getDicByKeyNo(String keyNo);


}
