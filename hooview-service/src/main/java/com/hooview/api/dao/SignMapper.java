package com.hooview.api.dao;

import com.hooview.api.entity.Sign;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SignMapper {
    int deleteByPrimaryKey(String id);

    int insert(Sign record);

    int insertSelective(Sign record);

    Sign selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Sign record);

    int updateByPrimaryKey(Sign record);

    Sign queryHead1ByUser(String uid);
}