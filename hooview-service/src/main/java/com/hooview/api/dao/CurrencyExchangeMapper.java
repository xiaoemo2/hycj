package com.hooview.api.dao;

import com.hooview.api.entity.CurrencyExchange;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CurrencyExchangeMapper {

    void insert(CurrencyExchange record);

    CurrencyExchange selectByPrimaryKey(Integer id);

    void updateByPrimaryKey(CurrencyExchange record);

    List<CurrencyExchange> queryByParams(Map<String, Object> params);

}