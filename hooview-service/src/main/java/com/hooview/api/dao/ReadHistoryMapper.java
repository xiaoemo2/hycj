package com.hooview.api.dao;

import com.hooview.api.entity.ReadHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ReadHistoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ReadHistory record);

    int insertSelective(ReadHistory record);

    ReadHistory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ReadHistory record);

    int updateByPrimaryKey(ReadHistory record);

    ReadHistory selectByNewsIdAndUid(@Param("newsId") Integer newsId,@Param("uid") String uid);
}