package com.hooview.api.dao;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Comment;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;
@Mapper
public interface CommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Comment record);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);

    Page<Map<String, Object>> findMapByPage(Map<String, Object> params);

    Page<Map<String,Object>> findByUser(String uid);

}