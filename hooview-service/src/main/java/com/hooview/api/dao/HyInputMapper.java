package com.hooview.api.dao;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyInput;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
@Mapper
public interface HyInputMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HyInput record);

    int insertSelective(HyInput record);

    HyInput selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HyInput record);

    int updateByPrimaryKey(HyInput record);

    HyInput findTodayHyInput(@Param("startTime") Date startTime,@Param("endTime") Date endTime);

    Page<HyInput> findAll(Date startTime);

    HyInput findByCreateTime(String createTime);
}