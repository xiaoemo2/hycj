package com.hooview.api.dao;

import com.hooview.api.entity.SignRule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SignRuleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SignRule record);

    int insertSelective(SignRule record);

    SignRule selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SignRule record);

    int updateByPrimaryKey(SignRule record);

    List<SignRule> findAll();

    SignRule findById(String id);

    SignRule findBySignDays(int signDays);
}