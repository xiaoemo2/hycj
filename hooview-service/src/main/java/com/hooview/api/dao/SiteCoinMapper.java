package com.hooview.api.dao;

import com.hooview.api.entity.SiteCoin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SiteCoinMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SiteCoin record);

    int insertSelective(SiteCoin record);

    SiteCoin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SiteCoin record);

    int updateByPrimaryKey(SiteCoin record);
}