package com.hooview.api.dao;

import com.hooview.api.entity.DailyCount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.sql.Date;
@Mapper
public interface DailyCountMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DailyCount record);

    int insertSelective(DailyCount record);

    DailyCount selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DailyCount record);

    int updateByPrimaryKey(DailyCount record);

    DailyCount selectByDate(@Param("date") Date date,@Param("uid") String uid);
}