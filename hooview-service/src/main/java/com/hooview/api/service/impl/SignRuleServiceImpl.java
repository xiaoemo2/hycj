package com.hooview.api.service.impl;

import com.hooview.api.dao.SignRuleMapper;
import com.hooview.api.entity.SignRule;
import com.hooview.api.service.SignRuleService;
import com.hooview.utils.SnowflakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Service("signRuleService")
public class SignRuleServiceImpl implements SignRuleService {
    @Autowired
    private SignRuleMapper signRuleMapper;
    @Autowired
    private SnowflakeIdWorker snowflakeIdWorker;

    @Override
    public List<SignRule> findAll() {
        return signRuleMapper.findAll();
    }

    @Override
    public List<SignRule> queryList(Map<String, Object> map) {
        return null;
    }

    @Override
    @Transactional(readOnly = false)
    public void save(@RequestBody SignRule signRule) {
        signRule.setId(getUUID());
        signRuleMapper.insert(signRule);
    }

    @Override
    @Transactional(readOnly = false)
    public void update(@RequestBody SignRule signRule) {
        signRule.setUpdateTime(new Date());
        signRuleMapper.updateByPrimaryKeySelective(signRule);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(String id) {
        signRuleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SignRule findById(String id) {
        return signRuleMapper.findById(id);
    }

    @Override
    public SignRule findBySigndays(int signDays) {
        return signRuleMapper.findBySignDays(signDays);
    }

    private String getUUID(){
        return String.valueOf(snowflakeIdWorker.nextId());
    }
}
