package com.hooview.api.service;

import com.hooview.api.entity.Likes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface LikesService {
    @RequestMapping(value = "/likes/insert",method = RequestMethod.GET)
    void insert(@RequestParam("userId") String userId, @RequestParam("newsId") Integer newsId,@RequestParam("type") String type);

    @RequestMapping(value = "/likes/selectByUserIdAndNewsId",method = RequestMethod.GET)
    Likes selectByUserIdAndNewsId(@RequestParam("userId") String userId, @RequestParam("newsId") Integer newsId);
}
