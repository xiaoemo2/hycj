package com.hooview.api.service.impl;

import com.hooview.api.dao.DictionaryMapper;
import com.hooview.api.entity.Dictionary;
import com.hooview.api.service.DictionaryService;
import com.hooview.utils.QueryFilters;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Service("dictionaryService")
public class DictionaryServiceImpl implements DictionaryService {
    @Resource
    private DictionaryMapper dictionaryMapper;

    @Override
    public List<Dictionary> getByFilter(@RequestBody QueryFilters filters) {
        return dictionaryMapper.getByFilter(filters);
    }

    @Override
    public Integer update(@RequestBody Dictionary entity) {
        return dictionaryMapper.update(entity);
    }

    @Override
    public Dictionary insert(@RequestBody Dictionary entity) {
        dictionaryMapper.insert(entity);
        return entity;
    }

    @Override
    public void delete(@RequestParam Integer id) {
        dictionaryMapper.delete(id);
    }

    @Override
    public List<Dictionary> getDicByKeyNo(@RequestParam String keyNo) {
        return dictionaryMapper.getDicByKeyNo(keyNo);
    }

}
