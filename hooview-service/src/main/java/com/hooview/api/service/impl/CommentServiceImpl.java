package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hooview.api.dao.CommentLikeMapper;
import com.hooview.api.dao.CommentMapper;
import com.hooview.api.entity.Comment;
import com.hooview.api.entity.CommentLike;
import com.hooview.api.service.CommentService;
import com.hooview.utils.SensitiveService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("commentService")
public class CommentServiceImpl implements CommentService {
    @Resource
    private CommentMapper commentMapper;
    @Resource
    private CommentLikeMapper commentLikeMapper;
    @Resource
    private SensitiveService sensitiveService;

    @Override
    public void delete(@RequestParam Long id) {
        commentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Long insert(@RequestBody Comment record) {
        String content = sensitiveService.filter(record.getContent());
        record.setContent(content);
        commentMapper.insert(record);
        return record.getId();
    }

    @Override
    public Comment selectByPrimaryKey(@RequestParam("id") Long id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = false)
    public int updateByPrimaryKeySelective(@RequestBody Comment record) {
        return commentMapper.updateByPrimaryKeySelective(record);
    }


    @Override
    public Page<Map<String, Object>> findMapByPage(@RequestBody Map<String, Object> params) {
        int pageNum=Integer.parseInt(params.get("pageNo").toString());
        int pageSize=Integer.parseInt(params.get("pageSize").toString());
        PageHelper.startPage(pageNum,pageSize,true,false);
        return commentMapper.findMapByPage(params);
    }

    @Override
    public void like(@RequestParam("uid") String uid, @RequestParam("commentId") Long commentId) {
        Comment comment = commentMapper.selectByPrimaryKey(commentId);
        Integer support = comment.getSupportAmount();
        Map params = new HashMap();
        params.put("userId", uid);
        params.put("commentId", commentId);
        List<CommentLike> commentLikes = commentLikeMapper.selectByParams(params);
        //已经点赞，则取消点赞
        if (commentLikes != null && commentLikes.size() > 0) {
            CommentLike commentLike = commentLikes.get(0);
            commentLikeMapper.deleteByPrimaryKey(commentLike.getId());

            if (support > 0) {
                support--;
            }
            comment.setSupportAmount(support);
            commentMapper.updateByPrimaryKey(comment);
            //点赞
        } else {
            support++;
            comment.setSupportAmount(support);
            commentMapper.updateByPrimaryKey(comment);
            CommentLike commentLike = new CommentLike();
            commentLike.setCommentId(commentId);
            commentLike.setUserId(uid);
            commentLikeMapper.insert(commentLike);
        }
    }

    @Override
    public  List<Map<String, Object>> findByUser(String uid, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize,true);
        PageHelper.orderBy("createTime desc");
        Page<Map<String, Object>> page = commentMapper.findByUser(uid);
        if(pageNum > page.getPages())
            return null;
        List<Map<String, Object>> result = page.getResult();
        for (Map<String,Object> r:result) {
            if(r.get("parentId") != null)
                r.put("type","回复了你的评论");
            else
                r.put("type","评论了你的文章");
        }
        return result;
    }
}
