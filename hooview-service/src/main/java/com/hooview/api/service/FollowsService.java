package com.hooview.api.service;

import com.hooview.api.entity.Follows;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface FollowsService {
    @RequestMapping(value = "/follow/selectByUidAndAutorId",method = RequestMethod.GET)
    Follows selectByUidAndAutorId(@RequestParam("uid") String uid, @RequestParam("authorId") String authorId);

    @RequestMapping(value = "/follow/insert",method = RequestMethod.GET)
    void insert(@RequestParam("uid") String uid,@RequestParam("authorId") String authorId);

    @RequestMapping(value = "/follow/deleteById",method = RequestMethod.GET)
    void deleteById(@RequestParam("id") Integer id);
}
