package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hooview.api.dao.TickerMapper;
import com.hooview.api.entity.Ticker;
import com.hooview.api.service.TickerService;
import com.hooview.utils.QueryFilters;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("tickerService")
public class TickerServiceImpl implements TickerService {
    @Resource
    private TickerMapper tickerMapper;

    @Override
    public int insert(@RequestBody Ticker record) {
        return tickerMapper.insert(record);
    }

    @Override
    public void deleteByParams(@RequestBody Map<String, Object> map) {
        tickerMapper.deleteByParams(map);
    }


    @Override
    public List<Ticker> queryByParams(@RequestBody Map<String, Object> map) {
        return tickerMapper.queryByParams(map);
    }

    @Override
    public List<Ticker> queryTrend(@RequestBody Map<String, Object> map) {
        return tickerMapper.queryTrend(map);
    }


}
