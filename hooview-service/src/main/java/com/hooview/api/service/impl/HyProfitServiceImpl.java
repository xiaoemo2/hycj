package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hooview.api.dao.HyProfitMapper;
import com.hooview.api.entity.HyProfit;
import com.hooview.api.service.HyProfitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HyProfitServiceImpl implements HyProfitService {
    @Autowired
    private HyProfitMapper hyProfitMapper;
    @Override
    public void insert(HyProfit hyProfit) {
        hyProfitMapper.insert(hyProfit);
    }

    @Override
    public Page<HyProfit> selectByUser(String uid, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize,true);
        PageHelper.orderBy("create_time desc");
        Page<HyProfit> page = hyProfitMapper.selectByUser(uid);
        if(pageNum > page.getPages())
            return null;
        return page;
    }
}
