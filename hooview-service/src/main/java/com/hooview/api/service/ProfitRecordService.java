package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.ProfitRecord;
import org.springframework.web.bind.annotation.*;
import sun.misc.Request;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 收益记录表
 */
@RestController
public interface ProfitRecordService {
    @RequestMapping(value = "/profitRecord/queryByUser",method = RequestMethod.GET)
    List<ProfitRecord> queryByUser(@RequestParam("uid") String uid, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);

    @RequestMapping(value = "/profitRecord/profitToday",method = RequestMethod.GET)
    Float profitToday(@RequestParam("uid") String uid);

    @RequestMapping(value = "/profitRecord/profitSum",method = RequestMethod.GET)
    Double profitSum(String uid);

    @RequestMapping(value = "/profitRecord/save",method = RequestMethod.POST)
    void save(@RequestBody ProfitRecord profitRecord);

    @RequestMapping(value = "/profitRecord/update",method = RequestMethod.POST)
    void update(ProfitRecord profitRecord);

    @RequestMapping(value = "/profitRecord/delete",method = RequestMethod.GET)
    void delete(String id);

    @RequestMapping(value = "/profitRecord/countByUser",method = RequestMethod.GET)
    Integer countByUser(String uid);

    @RequestMapping(value = "/profitRecord/findByUserAndNewsId",method = RequestMethod.GET)
    ProfitRecord findByUserAndNewsId(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId);

    /**
     * 获取收益通用服务
     * @param uid
     * @param type 00:注册 01：签到  02：阅读 03：分享快讯 04:分享文章 05：一级用户收益 06：二级好友收益 07：利好 08：利空
     * @return
     */
    @RequestMapping(value = "/profitRecord/getProfit",method = RequestMethod.GET)
    Double getProfit(String uid,String type,Integer newsId);

    /**
     * 查询news是否已分享 阅读
     * @param uid
     * @param newsId
     * @param type
     * @return
     */
    @RequestMapping(value = "/profitRecord/findByUserAndNewsIdAndType",method = RequestMethod.GET)
    ProfitRecord findByUserAndNewsIdAndType(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId,@RequestParam("type") String type);

    /**
     * 今日所有用户获取的火力值
     * @return
     */
    Float todayToalProfits();
}
