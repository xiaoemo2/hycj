package com.hooview.api.service;

import com.hooview.api.entity.Collections;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface CollectionsService {
    @RequestMapping(value = "/collections/selectByUidAndNewsId",method = RequestMethod.GET)
    Collections selectByUidAndNewsId(@RequestParam("uid") String uid, @RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/collections/insert",method = RequestMethod.GET)
    void insert(@RequestParam("uid") String uid,@RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/collections/deleteById",method = RequestMethod.GET)
    void deleteById(@RequestParam("id") Integer id);
}
