package com.hooview.api.service.impl;

import com.hooview.api.dao.SignMapper;
import com.hooview.api.entity.Sign;
import com.hooview.api.service.SignService;
import com.hooview.utils.SnowflakeIdWorker;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
@Service
public class SignServiceImpl implements SignService {
    @Autowired
    private SignMapper signMapper;
    @Autowired
    private SnowflakeIdWorker snowflakeIdWorker;
    private static Logger logger = LoggerFactory.getLogger(SignServiceImpl.class);
    @Override
    public Sign queryByUser(String uid) {
        return null;
    }

    @Override
    public List<Sign> queryList(Map<String, Object> map) {
        return null;
    }

    @Override
    @Transactional(readOnly = false)
    public void save(@RequestBody Sign sign) {
        if(StringUtils.isBlank(sign.getId())) sign.setId(String.valueOf(snowflakeIdWorker.nextId()));
        signMapper.insertSelective(sign);
    }

    @Override
    public void update(Sign sign) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public Sign queryHead1ByUser(String uid) {
        return signMapper.queryHead1ByUser(uid);
    }

    @Override
    @Transactional(readOnly = false)
    public void clearSignTimes(@RequestBody Sign sign) {
        signMapper.updateByPrimaryKeySelective(sign);
    }
}
