package com.hooview.api.service.impl;

import com.hooview.api.dao.CurrencyExchangeMapper;
import com.hooview.api.entity.CurrencyExchange;
import com.hooview.api.service.CurrencyExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service("currencyExchangeService")
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
    @Resource
    private CurrencyExchangeMapper currencyExchangeMapper;

    @Override
    public void insert(@RequestBody CurrencyExchange record) {
        Map map = new HashMap();
        map.put("scur", record.getScur());
        map.put("tcur", record.getTcur());
        List<Map<String, Object>> list = currencyExchangeMapper.queryByParams(map);
        String key = record.getScur() + "-" + record.getTcur();
        if (list.isEmpty()) {
            currencyExchangeMapper.insert(record);
        } else {
            record.setId(Integer.parseInt(list.get(0).get("id").toString()));
            currencyExchangeMapper.updateByPrimaryKey(record);
        }
    }


    @Override
    public List<CurrencyExchange> queryByParams(@RequestBody Map<String, Object> map) {
        return currencyExchangeMapper.queryByParams(map);
    }

    @Override
    public Double queryRate(String scur, String tcur) {
        String key = scur + "-" + tcur;
        Map currencyParams = new HashMap();
        //scur:源币种 tcur：目标币种
        currencyParams.put("scur", scur);
        currencyParams.put("tcur", tcur);
        List<Map<String, Object>> list = currencyExchangeMapper.queryByParams(currencyParams);
        if (list.isEmpty()) {
            return 0D;
        } else {
            if (list.get(0).get("rate") != null) {
                String rate = list.get(0).get("rate").toString();
                return Double.parseDouble(rate);
            }
        }
        return 0D;
    }

    @Override
    public void updateByPrimaryKey(CurrencyExchange record) {

    }


}
