package com.hooview.api.service.impl;

import com.hooview.api.dao.ReadHistoryMapper;
import com.hooview.api.entity.ReadHistory;
import com.hooview.api.service.ReadHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class ReadHistoryServiceImpl implements ReadHistoryService {
    @Autowired
    private ReadHistoryMapper readHistoryMapper;
    @Override
    public void insert(String uid, Integer newsId) {
        ReadHistory readHistory = new ReadHistory();
        readHistory.setCreateTime(new Date());
        readHistory.setNewsId(newsId);
        readHistory.setUserId(uid);
        readHistoryMapper.insert(readHistory);
    }

    @Override
    public ReadHistory selectByNewsIdAndUid(Integer newsId, String uid) {
        return readHistoryMapper.selectByNewsIdAndUid(newsId,uid);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteById(Integer id) {
        readHistoryMapper.deleteByPrimaryKey(id);
    }
}
