package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Coin;
import com.hooview.utils.QueryFilters;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public interface CoinService {
    @RequestMapping(value = "/coin/delete", method = RequestMethod.POST)
    int deleteByPrimaryKey(Integer id);

    @RequestMapping(value = "/coin/save", method = RequestMethod.POST)
    int insert(@RequestBody Coin record);

    @RequestMapping(value = "/coin/queryObject", method = RequestMethod.GET)
    Coin selectByPrimaryKey(@RequestParam("id") Integer id);

    @RequestMapping(value = "/coin/update", method = RequestMethod.POST)
    int updateByPrimaryKeySelective(@RequestBody Coin record);

    @RequestMapping(value = "/coin/queryList", method = RequestMethod.POST)
    List<Coin> queryByParams(@RequestBody Map<String, Object> map);

}