package com.hooview.api.service;

import com.hooview.api.entity.SignRule;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
@RestController
public interface SignRuleService {

    @RequestMapping(value = "/api/signRule/findAll",method = RequestMethod.GET)
    List<SignRule> findAll();
    List<SignRule> queryList(Map<String, Object> map);
    @RequestMapping(value = "/api/signRule/save",method = RequestMethod.POST)
    void save(@RequestBody SignRule signRule);
    @RequestMapping(value = "/api/signRule/update",method = RequestMethod.POST)
    void update(@RequestBody SignRule signRule);
    @RequestMapping(value = "/api/signRule/delete",method = RequestMethod.GET)
    void delete(String id);
    @RequestMapping(value = "/api/signRule/findById",method = RequestMethod.GET)
    SignRule findById(String id);
    @RequestMapping(value = "/api/signRule/findBySigndays",method = RequestMethod.GET)
    SignRule findBySigndays(@RequestParam("signDays") int signDays);
}
