package com.hooview.api.service;

import com.hooview.api.entity.Banner;
import com.hooview.utils.QueryFilters;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public interface BannerService {

    @RequestMapping(value = "/banner/queryObject", method = RequestMethod.GET)
    Banner queryObject(@RequestParam(value = "id") Integer id);

    @RequestMapping(value = "/banner/queryList", method = RequestMethod.POST)
    List<Banner> queryList(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/banner/save", method = RequestMethod.POST)
    void save(@RequestBody Banner banner);

    @RequestMapping(value = "/banner/update", method = RequestMethod.POST)
    void update(@RequestBody Banner banner);

    @RequestMapping(value = "/banner/delete", method = RequestMethod.POST)
    void delete(@RequestParam("id") Integer id);

    @RequestMapping(value = "/banner/getByFilter", method = RequestMethod.GET)
    List<Banner> getByFilter(@RequestBody QueryFilters filters);
}
