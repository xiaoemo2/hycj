package com.hooview.api.service.impl;

import com.hooview.api.dao.CoinMapper;
import com.hooview.api.entity.Coin;
import com.hooview.api.service.CoinService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("coinService")
public class CoinServiceImpl implements CoinService {
    @Resource
    private CoinMapper coinMapper;

    @Override
    public int deleteByPrimaryKey(@RequestParam Integer id) {
        return coinMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(@RequestBody Coin record) {
        coinMapper.insert(record);
        return record.getId();
    }

    @Override
    public Coin selectByPrimaryKey(@RequestParam("id") Integer id) {
        return coinMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = false)
    public int updateByPrimaryKeySelective(@RequestBody Coin record) {
        return coinMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<Coin> queryByParams(@RequestBody Map<String, Object> map) {
        return coinMapper.queryByParams(map);
    }
}
