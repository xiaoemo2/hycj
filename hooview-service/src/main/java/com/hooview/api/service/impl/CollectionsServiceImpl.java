package com.hooview.api.service.impl;

import com.hooview.api.dao.CollectionsMapper;
import com.hooview.api.entity.Collections;
import com.hooview.api.service.CollectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CollectionsServiceImpl implements CollectionsService{
    @Autowired
    private CollectionsMapper collectionsMapper;
    @Override
    public Collections selectByUidAndNewsId(String uid, Integer newsId) {
        return collectionsMapper.selectByUidAndNewsId(uid,newsId);
    }

    @Override
    @Transactional(readOnly = false)
    public void insert(String uid, Integer newsId) {
        Collections collection = new Collections();
        collection.setUserId(uid);
        collection.setNewsId(newsId);
        collectionsMapper.insert(collection);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteById(Integer id) {
        collectionsMapper.deleteByPrimaryKey(id);
    }
}
