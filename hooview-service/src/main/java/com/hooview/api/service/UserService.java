package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hooview.api.entity.Sign;
import com.hooview.api.entity.User;
import com.hooview.api.entity.dto.AuthorSimpleInfo;
import com.hooview.api.entity.dto.UserDTO;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
@RestController
@RequestMapping("/api")
public interface UserService {
    Sign queryObject(String id);

    List<User> queryList(Map<String, Object> map);

    void save(User user);

    /**
     * 用户信息更新
     * @param user
     */
    @RequestMapping(value = "/user/update",method = RequestMethod.POST)
    void update(@RequestBody User user);

    void delete(String id);

    /**
     * 用户注册
     * @param user
     */
    @RequestMapping(value = "/user/register",method = RequestMethod.POST)
    void register(@RequestBody User user);

    /**
     * 账号查询
     * @param username
     * @return
     */
    @RequestMapping(value = "/user/queryByUsername",method = RequestMethod.GET)
    User queryByUsername(String username);

    /**
     * id查询
     * @param uid
     * @return
     */
    @RequestMapping(value = "/user/queryById",method = RequestMethod.GET)
    User queryById(@RequestParam("uid") String uid);

    /**
     * 生成Token
     * @param user
     * @return
     */
    @RequestMapping(value = "/user/createToken",method = RequestMethod.POST)
    String createToken(@RequestBody User user);

    /**
     * 退出
     * @param token
     */
    @RequestMapping(value = "/user/logout",method = RequestMethod.GET)
    void logout(@RequestParam("token") String token);

    /**
     * openId查询
     * @param openId
     * @return
     */
    @RequestMapping(value = "/user/queryByOpenId",method = RequestMethod.GET)
    User queryByOpenId(String openId);

    /**
     * 运营后台查询
     * params：
     * mobile=1 openId=1 表示手机 openId不为空
     * media：00 不是自媒体 01：是
     * mediaType ：00 非平台自媒体 01：平台自媒体
     * 其他参数包含 allCandy todayCandy invitatNum
     * pageNum 页码
     * pageSize 每页显示数量
     * 所有条件并列查询
     */
    @RequestMapping(value = "/user/queryByParams",method = RequestMethod.POST)
    Map queryByParams(@RequestBody Map<String,Object> params, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);

    /**
     * 查询自媒体用户
     * @param
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/user/queryMediaUser",method = RequestMethod.POST)
    Map queryMediaUserByParams(@RequestBody Map<String, Object> params,@RequestParam(name = "pageNum") Integer pageNum,@RequestParam(name = "pageSize") Integer pageSize);

    /**
     * 查询作者信息
     * @param authorId
     * @return
     */
    @RequestMapping(value = "/user/queryAuthorInfo",method = RequestMethod.GET)
    AuthorSimpleInfo queryAuthorInfo(@RequestParam("authorId") String authorId);

    /**
     * 查询所有用户
     * @return
     */
    List<User> findAll();

    /**
     * 查询邀请的用户
     * @param uid
     * @param type 00：全部 01：一级 02：二级
     * @return
     */
    @RequestMapping(value = "/user/queryInvitedFriends",method = RequestMethod.GET)
    Page<User> queryInvitedFriends(@RequestParam("uid") String uid, @RequestParam("type") String type, @RequestParam("pageNum") Integer pageNum, @RequestParam(name = "pageSize") Integer pageSize);

    /**
     * 今日能量池信息
     * @return
     */
    @RequestMapping(value = "/user/queryTodayEnergyPoolInfo",method = RequestMethod.GET)
    Map<String,Object> queryTodayEnergyPoolInfo(@RequestParam("uid") String uid);

    /**
     * 查询当前用户邀请人数
     */
    @RequestMapping(value = "/user/countInvite",method = RequestMethod.GET)
    Integer countInvite(@RequestParam("uid") String uid);

    /**
     * 推荐作者
     */
    @RequestMapping(value = "/user/recommendAuthor",method = RequestMethod.POST)
    Map recommendAuthor(@RequestBody Map<String, Object> params,@RequestParam(name = "pageNum") Integer pageNum,@RequestParam(name = "pageSize") Integer pageSize);

}
