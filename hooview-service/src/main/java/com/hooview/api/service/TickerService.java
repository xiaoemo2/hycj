package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Ticker;
import com.hooview.utils.QueryFilters;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public interface TickerService {

    @RequestMapping(value = "/ticker/save", method = RequestMethod.POST)
    int insert(@RequestBody Ticker record);

    @RequestMapping(value = "/ticker/deleteByParams", method = RequestMethod.POST)
    void deleteByParams(@RequestBody Map<String, Object> map);


    @RequestMapping(value = "/ticker/queryByParams", method = RequestMethod.POST)
    List<Ticker> queryByParams(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/ticker/queryTrend", method = RequestMethod.POST)
    List<Ticker> queryTrend(@RequestBody Map<String, Object> map);

}