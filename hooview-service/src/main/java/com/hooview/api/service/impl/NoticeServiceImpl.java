package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hooview.api.dao.NoticesMapper;
import com.hooview.api.entity.Notices;
import com.hooview.api.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    private NoticesMapper noticesMapper;

    @Override
    public Page<Notices> findByType(String type, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize,true,false);
        PageHelper.orderBy("create_time desc");
        Page<Notices> page = noticesMapper.findByType(type);
        return page;
    }

    @Override
    @Transactional(readOnly = false)
    public void insert(@RequestBody Notices notices) {
        noticesMapper.insert(notices);
    }

    @Override
    @Transactional(readOnly = false)
    public void update(@RequestBody Notices notices) {
        noticesMapper.updateByPrimaryKeySelective(notices);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        noticesMapper.deleteByPrimaryKey(id);
    }

}
