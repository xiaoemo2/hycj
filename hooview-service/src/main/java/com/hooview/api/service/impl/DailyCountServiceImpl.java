package com.hooview.api.service.impl;

import com.hooview.api.dao.DailyCountMapper;
import com.hooview.api.entity.DailyCount;
import com.hooview.api.service.DailyCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.Date;

@Service
public class DailyCountServiceImpl implements DailyCountService {
    @Autowired
    private DailyCountMapper dailyCountMapper;

    @Override
    public DailyCount selectToday(String uid) {
        return dailyCountMapper.selectByDate(new Date(System.currentTimeMillis()),uid);
    }

    @Override
    @Transactional(readOnly = false)
    public void insert(@RequestBody DailyCount dailyCount) {
        dailyCountMapper.insert(dailyCount);
    }

    @Override
    @Transactional(readOnly = false)
    public void updateByPrimaryKeySelective(@RequestBody DailyCount dailyCount) {
        dailyCountMapper.updateByPrimaryKeySelective(dailyCount);
    }
}
