package com.hooview.api.service;

import com.hooview.api.entity.CurrencyExchange;
import com.hooview.api.entity.Ticker;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public interface CurrencyExchangeService {

    @RequestMapping(value = "/currencyExchange/save", method = RequestMethod.POST)
    void insert(@RequestBody CurrencyExchange record);


    @RequestMapping(value = "/currencyExchange/queryByParams", method = RequestMethod.POST)
    List<CurrencyExchange> queryByParams(@RequestBody Map<String, Object> map);

    /**
     * 获取汇率
     *
     * @param scur:源币种
     * @param tcur：目标币种
     * @return 汇率
     */
    Double queryRate(String scur, String tcur);

    void updateByPrimaryKey(CurrencyExchange record);
}