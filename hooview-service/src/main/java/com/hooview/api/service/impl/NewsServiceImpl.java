package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hooview.api.dao.NewsMapper;
import com.hooview.api.entity.News;
import com.hooview.api.service.NewsService;
import com.hooview.utils.QueryFilters;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("newsService")
public class NewsServiceImpl implements NewsService {
    @Resource
    private NewsMapper newsMapper;

    @Override
    public int deleteByPrimaryKey(@RequestParam Integer id) {
        return newsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(@RequestBody News record) {
        newsMapper.insert(record);
        return record.getId();
    }

    @Override
    public News selectByPrimaryKey(@RequestParam("id") Integer id) {
        return newsMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = false)
    public int updateByPrimaryKeySelective(@RequestBody News record) {
        return newsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<News> queryByParams(@RequestBody Map<String, Object> map) {
        return newsMapper.queryByParams(map);
    }

    @Override
    public Page<News> getByFilter(@RequestBody QueryFilters filters) {
        Page page = PageHelper.startPage(filters.getPageNo(), filters.getPageSize());
        filters.setSidx("id");
        Page<News> list = newsMapper.getByFilter(filters);
        PageInfo<News> pageInfo = new PageInfo<>(list);
        return list;
    }
    @Override
    public Page<Map<String,Object>> getMapByFilter(@RequestBody QueryFilters filters) {
        Page page = PageHelper.startPage(filters.getPageNo(), filters.getPageSize());
        filters.setSidx("id");
        Page<Map<String,Object>> list = newsMapper.getMapByFilter(filters);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(list);
        return list;
    }

    @Override
    public List<News> getByFilter4Api(@RequestBody QueryFilters filters) {
        return newsMapper.getByFilter(filters);
    }

    @Override
    public Integer getCountByFilter(@RequestBody QueryFilters filters) {
        return newsMapper.getCountByFilter(filters);
    }

    @Override
    public Integer queryNewsCount(@RequestBody Map<String, Object> map) {
        return newsMapper.queryNewsCount(map);
    }

    @Override
    public Page<News> findByPage(@RequestBody Map<String, Object> params) {
        return newsMapper.findByPage(params);
    }

    @Override
    public Page<Map<String, Object>> findMapByPage(@RequestBody  Map<String, Object> params) {
        return newsMapper.findMapByPage(params);
    }
    @Override
    public Page<Map<String, Object>> findFollowMapByPage(@RequestBody  Map<String, Object> params) {
        return newsMapper.findFollowMapByPage(params);
    }

    @Override
    public Map<String, Object> selectMapByPrimaryKey(@RequestBody Map<String, Object> params) {
        return newsMapper.selectMapByPrimaryKey(params);
    }
    @Override
    public Page<Map<String, Object>> queryHistoriesByPage(@RequestBody Map<String, Object> params) {
        return newsMapper.queryHistoriesByPage(params);
    }

    @Override
    public Page<Map<String, Object>> queryHistoriesByPageNo(@RequestBody Map<String, Object> params) {
        int pageNo=Integer.parseInt(params.get("pageNo").toString());
        int pageSize=Integer.parseInt(params.get("pageSize").toString());
        PageHelper.startPage(pageNo, pageSize,true,false);
        return newsMapper.queryHistoriesByPageNo(params);
    }
    @Override
    public Page<Map<String, Object>> queryRecommendsByPage(@RequestBody Map<String, Object> params) {
        return newsMapper.queryRecommendsByPage(params);
    }

    @Override
    public Page<Map<String, Object>> queryCollectionsByPage(@RequestBody Map<String, Object> params) {
        return newsMapper.queryCollectionsByPage(params);
    }
    @Override
    public Page<Map<String, Object>> queryAuthorByPage(@RequestBody Map<String, Object> params) {
        return newsMapper.queryAuthorByPage(params);
    }

    @Override
    public List<Map<String, Object>> findByKeyword(String keyword, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize,true,false);
        return newsMapper.findByKeyword(keyword);
    }

    @Override
    public Map<String, Object> findNextNews(Integer currentId) {
        News news = newsMapper.selectByPrimaryKey(currentId);
        return newsMapper.findNextNews(currentId,news.getChannelId());
    }

    @Override
    public List<Map<String, Object>> findRecommendNews(Integer currentId) {
        News news = newsMapper.selectByPrimaryKey(currentId);
        return newsMapper.findRecommendNews(news.getChannelId());
    }
}
