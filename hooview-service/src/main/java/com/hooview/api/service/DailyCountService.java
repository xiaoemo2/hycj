package com.hooview.api.service;

import com.hooview.api.entity.DailyCount;
import org.springframework.web.bind.annotation.*;

@RestController
public interface DailyCountService {
    @RequestMapping(value = "/dailyCount/selectToday",method = RequestMethod.GET)
    @ResponseBody
    DailyCount selectToday(String uid);

    @RequestMapping(value = "/dailyCount/insert",method = RequestMethod.POST)
    void insert(@RequestBody DailyCount dailyCount);

    @RequestMapping(value = "/dailyCount/updateByPrimaryKeySelective",method = RequestMethod.POST)
    void updateByPrimaryKeySelective(@RequestBody DailyCount dailyCount);
}
