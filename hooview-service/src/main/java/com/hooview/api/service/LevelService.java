package com.hooview.api.service;

import com.hooview.api.entity.Level;

import java.util.List;
import java.util.Map;

public interface LevelService {

    Level queryObject(String id);

    List<Level> queryList(Map<String, Object> map);

    void save(Level level);

    void update(Level level);

    void delete(String id);
}
