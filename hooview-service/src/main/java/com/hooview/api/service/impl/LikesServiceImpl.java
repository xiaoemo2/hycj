package com.hooview.api.service.impl;

import com.hooview.api.dao.LikesMapper;
import com.hooview.api.entity.Likes;
import com.hooview.api.service.LikesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class LikesServiceImpl implements LikesService {
    @Autowired
    private LikesMapper likesMapper;
    @Override
    @Transactional(readOnly = false)
    public void insert(String userId, Integer newsId,String type) {
        Likes likes = new Likes();
        likes.setCreateTime(new Date());
        likes.setUserId(userId);
        likes.setNewsId(newsId);
        likes.setType(type);
        likesMapper.insert(likes);
    }

    @Override
    public Likes selectByUserIdAndNewsId(String userId, Integer newsId) {
        return likesMapper.selectByUserIdAndNewsId(userId,newsId);
    }
}
