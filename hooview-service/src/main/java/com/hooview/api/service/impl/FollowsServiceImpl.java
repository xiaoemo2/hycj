package com.hooview.api.service.impl;

import com.hooview.api.dao.FollowsMapper;
import com.hooview.api.entity.Follows;
import com.hooview.api.service.FollowsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FollowsServiceImpl implements FollowsService {
    @Autowired
    private FollowsMapper followsMapper;
    @Override
    public Follows selectByUidAndAutorId(String uid, String authorId) {
        return followsMapper.selectByUidAndAutorId(uid,authorId);
    }

    @Override
    @Transactional(readOnly = false)
    public void insert(String uid, String authorId) {
        Follows follow = new Follows();
        follow.setUserId(uid);
        follow.setFollowerId(authorId);
        followsMapper.insert(follow);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteById(Integer id) {
        followsMapper.deleteByPrimaryKey(id);
    }
}
