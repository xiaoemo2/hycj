package com.hooview.api.service;

import com.hooview.api.entity.Sign;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
@RestController
public interface SignService {

    Sign queryByUser(String uid);

    List<Sign> queryList(Map<String, Object> map);

    void update(Sign sign);

    void delete(String id);

    /**
     * 用户最近签到详情
     * @param uid
     * @return
     */
    @RequestMapping(value = "/sign/queryHead1",method = RequestMethod.GET)
    Sign queryHead1ByUser(String uid);

    /**
     * 清空用户连续签到次数
     * @param sign
     */
    @RequestMapping(value = "/sign/clearSignTimes",method = RequestMethod.POST)
    void clearSignTimes(Sign sign);

    /**
     * 签到保存
     * @param sign
     */
    @RequestMapping(value = "/sign/save",method = RequestMethod.POST)
    void save(@RequestBody Sign sign);
}
