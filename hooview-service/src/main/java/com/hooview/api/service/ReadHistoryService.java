package com.hooview.api.service;

import com.hooview.api.entity.ReadHistory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface ReadHistoryService {
    @RequestMapping(value = "/readHistory/insert",method = RequestMethod.GET)
    void insert(@RequestParam("uid") String uid, @RequestParam("newsId") Integer newsId);

    @RequestMapping(value = "/readHistory/selectByNewsIdAndUid",method = RequestMethod.GET)
    ReadHistory selectByNewsIdAndUid(@RequestParam("newsId") Integer newsId, @RequestParam("uid") String uid);

    @RequestMapping(value = "/readHistory/deleteById",method = RequestMethod.GET)
    void deleteById(@RequestParam("id") Integer id);
}
