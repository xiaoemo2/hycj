package com.hooview.api.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hooview.api.dao.HyInputMapper;
import com.hooview.api.entity.HyInput;
import com.hooview.api.service.HyInputService;
import com.hooview.utils.DateUtils;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.omg.CORBA.DATA_CONVERSION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springside.modules.utils.time.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Service
public class HyInputServiceImpl implements HyInputService{
    @Autowired
    private HyInputMapper hyInputMapper;
    @Override
    public HyInput findTodayHyInput() {
        return hyInputMapper.findByCreateTime(DateUtils.formatTime(new Date()));
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean insert(@RequestBody HyInput hyInput) {
        Date createTime = hyInput.getCreateTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(createTime);
        HyInput hi =  hyInputMapper.findByCreateTime(format);
        if(hi != null)
            return false;
        hyInputMapper.insert(hyInput);
        return true;
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean delete(Integer id) {
        HyInput hyInput = hyInputMapper.selectByPrimaryKey(id);
        String s = DateUtils.formatDate(new Date());
        String s1 = DateUtils.formatDate(hyInput.getCreateTime());
        if(new Date().before(hyInput.getCreateTime()) || s.equals(s1)){
            hyInputMapper.deleteByPrimaryKey(id);
            return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean update(@RequestBody HyInput hyInput) {
        String s = DateUtils.formatDate(new Date());
        String s1 = DateUtils.formatDate(hyInput.getCreateTime());
        if(new Date().before(hyInput.getCreateTime()) || s.equals(s1)){
            hyInputMapper.updateByPrimaryKeySelective(hyInput);
            return true;
        }
        return false;
    }

    @Override
    public HyInput findById(Integer id) {
        return hyInputMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<HyInput> findAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize,true,false);
        PageHelper.orderBy("create_time desc");
        //前7天  后所有
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DAY_OF_MONTH,-7);
        instance.set(Calendar.HOUR_OF_DAY,0);
        instance.set(Calendar.MINUTE,0);
        instance.set(Calendar.SECOND,0);
        Date startTime = instance.getTime();

        return hyInputMapper.findAll(startTime);
    }

}
