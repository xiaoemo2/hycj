package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyProfit;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface HyProfitService {
    void insert(HyProfit hyProfit);

    @RequestMapping(value = "/hyProfit/selectByUser",method = RequestMethod.GET)
    Page<HyProfit> selectByUser(@RequestParam("uid") String uid, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);

}
