package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.HyInput;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/hyInput")
public interface HyInputService {
    HyInput findTodayHyInput();

    /**
     * 投放 createTime 字段为使用时间
     *  使用时间不存在则 成功 true 否则 失败false
     * @param hyInput
     */
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    Boolean insert(@RequestBody HyInput hyInput);

    /**
     * 根据id删除
     * 使用时间在当前时间后删除成功返回true 否则失败 false
     * @param id
     */
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    Boolean delete(@RequestParam("id") Integer id);

    /**
     * 更新
     * 使用时间在当前时间后成功 true 否则失败 false
     * @param hyInput
     */
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    Boolean update(@RequestBody HyInput hyInput);

    /**
     * 根据 id查询
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById",method = RequestMethod.GET)
    HyInput findById(@RequestParam("id") Integer id);

    /**
     * 投放记录列表 前七天 后面所有
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/findAll",method = RequestMethod.GET)
    Page<HyInput> findAll(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);
}
