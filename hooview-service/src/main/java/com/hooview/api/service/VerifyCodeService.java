package com.hooview.api.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface VerifyCodeService {
    @RequestMapping(value = "/verifyCode/genCode",method = RequestMethod.GET)
    String genCode(String mobile);

    @RequestMapping(value = "/verifyCode/isValidCode",method = RequestMethod.GET)
    Boolean isValidCode(String mobile, String code);

    @RequestMapping(value = "/verifyCode/deleteCode",method = RequestMethod.GET)
    void deleteCode(String mobile);
}
