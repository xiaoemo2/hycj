package com.hooview.api.service;

import com.github.pagehelper.Page;
import com.hooview.api.entity.Notices;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public interface NoticeService {

    @RequestMapping(value = "/notice/findByType",method = RequestMethod.GET)
    Page<Notices> findByType(@RequestParam("type") String type, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize);

    @RequestMapping(value = "/notice/insert",method = RequestMethod.POST)
    void insert(@RequestBody Notices notices);

    @RequestMapping(value = "/notice/update",method = RequestMethod.POST)
    void update(@RequestBody Notices notices);

    @RequestMapping(value = "/notice/delete",method = RequestMethod.GET)
    void delete(@RequestParam("id") Integer id);

}
