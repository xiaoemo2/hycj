package com.hooview.utils;


/**
 * Created by Administrator on 2015/6/21.
 */
public class QueryRule {
    private String field;
    // odata: [{ oper:'eq', text:'等于'},{ oper:'ne', text:'不等'},{ oper:'lt', text:'小于'},{ oper:'le', text:'小于等于'},
    // { oper:'gt', text:'大于\u3000\u3000'},{ oper:'ge', text:'大于等于'},{ oper:'bw', text:'开始于'},
    // { oper:'bn', text:'不开始于'},{ oper:'in', text:'属于\u3000\u3000'},{ oper:'ni', text:'不属于'},
    // { oper:'ew', text:'结束于'},{ oper:'en', text:'不结束于'},{ oper:'cn', text:'包含\u3000\u3000'},
    // { oper:'nc', text:'不包含'},{ oper:'nu', text:'不存在'},{ oper:'nn', text:'存在'}],
    private String data;
    private String op="cn";
    private String groupOp="AND";
    public QueryRule(){}
    public QueryRule(String filed,String data,String op,String groupOp){
        this.field=filed;
        this.data=data;
        this.op=op; this.groupOp=groupOp;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(String groupOp) {
        this.groupOp = groupOp;
    }
}
