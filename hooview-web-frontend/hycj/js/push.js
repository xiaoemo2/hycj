(function ($) {
    var page = "push";
    layui.use(['element', 'form', 'layer', 'laytpl', 'laydate'], function () {
        var model = $(".push-content");
        var layer = layui.layer;
        var form = layui.form;
        var laytpl = layui.laytpl;
        var laydate = layui.laydate;
        //是否有任务在执行
        var processing = false;

        //新增
        form.on('submit(addPushForm)', function (data) {
            if (processing || !data.form) {
                return false
            }
            processing = true;

            var formData = data.field;
            $.getJsonData("/hycj/push", formData, {
                type: "Post",
                contentType: "application/x-www-form-urlencoded;charset=utf-8"
            }).done(function (saveResult) {
                if (saveResult.code != "200") {
                    layer.msg(saveResult.message, {icon: 2});
                    processing = false;
                } else {
                    layer.msg('推送成功', {icon: 6});
                }
                processing = false;
            }).fail(function () {
                processing = false;
            });

            return false;
        });


        form.verify({
            integer: function (value, item) {
                if (value) {
                    try {
                        if (parseInt(value) != value) {
                            return "请输入整数";
                        }
                    } catch (error) {
                        return "请输入整数";
                    }
                }
            },
            positiveInteger: function (value, item) {//正整数
                if (value) {
                    try {
                        if (parseInt(value) != value || value <= 0) {
                            return "请输入正整数";
                        }
                    } catch (error) {
                        return "请输入正整数";
                    }
                }
            }
        });
    });
})(jQuery);