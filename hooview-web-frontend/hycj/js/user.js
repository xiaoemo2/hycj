(function ($) {
    var page = "user";
    layui.use(['element', 'form', 'layer', 'laytpl'], function () {
        var model = $(".user-content");
        var layer = layui.layer;
        var form = layui.form;
        var laytpl = layui.laytpl;

        //是否有任务在执行
        var processing = false;

        form.render("checkbox");
        var table = $(".user-table").table({
            columns: [
                {
                    "name": "displayName",
                    "display": "昵称"
                },
                {
                    "name": "mobile",
                    "display": "手机号"
                },
                {
                    "name": "refereeName",
                    "display": "邀请人"
                },
                {
                    "name": "allCandy",
                    "display": "HY数量"
                },
                {
                    "name": "todayCandy",
                    "display": "今日火力值"
                },
                {
                    "name": "invitatNum",
                    "display": "邀请人数"
                },
                {
                    "display": "自媒体账户",
                    "align": "center",
                    "render": function (rowData) {
                        if (rowData.media == '01') {
                            return "<span class='ok-icon'><i class=\"layui-icon\">&#xe605;</i></span>";
                        }
                    }
                },
                {
                    "display": "平台自媒体账户",
                    "align": "center",
                    "render": function (rowData) {
                        if (rowData.mediaType == '01') {
                            return "<span class='ok-icon'><i class=\"layui-icon\">&#xe605;</i></span>";
                        }
                    }
                },
                {
                    "display": "操作",
                    render: function (rowData) {
                        return "<div class='opt-btn-group'><span class='edit-item'><i class=\"layui-icon\">&#xe642;</i>编辑</span></div>";
                    }
                }
            ],
            url: "/hycj/users",
            showTitle: false,
            formatData: function (data) {
                return data.datas;
            }
        });

        //编辑对话框
        model.on("click", ".edit-item", function (e) {
            var rowData = table.getRowData($(e.target).closest("tr"));
            laytpl($("#userForm").html()).render({
                model: "updateUserForm",
                data: rowData
            }, function (content) {
                layer.open({
                    type: 1,
                    title: "更新用户",
                    content: content,
                    area: "400px",
                    maxHeight: app.clientHeight() - 50,
                    success: function () {
                        form.render("checkbox");
                    }
                });
            });
        });

        //搜索
        form.on('submit(searchUserForm)', function (data) {
            if (!data.form) {
                return false
            }
            // if (data.field.mobile == "on") {
            //     data.field.mobile = 1;
            // } else {
            //     data.field.mobile = null;
            // }
            // if (data.field.openId == "on") {
            //     data.field.openId = 1;
            // } else {
            //     data.field.openId = null;
            // }
            if (data.field.media == "on") {
                data.field.media = "01";
            } else {
                data.field.media = null;
            }
            if (data.field.mediaType == "on") {
                data.field.mediaType = "01";
            } else {
                data.field.mediaType = null;
            }
            data.field.pageNum = 1;
            table.refresh(data.field);
            return false;
        });

        //勾选平台自媒体，自动勾选上自媒体账户
        form.on('checkbox(mediaType)', function (data) {
            if (data.elem.checked) {
                $(".user-form [name='media']").prop("checked", true);
                form.render("checkbox");
            }
        });

        //取消勾选自媒体，自动取消勾选上平台自媒体账户
        form.on('checkbox(media)', function (data) {
            if (!data.elem.checked) {
                $(".user-form [name='mediaType']").prop("checked", false);
                form.render("checkbox");
            }
        });

        //更新
        form.on('submit(updateUserForm)', function (data) {
            if (!data.form) {
                return false
            }
            if (data.field.media == "on") {
                data.field.media = "01";
            } else {
                data.field.media = "00";
            }
            if (data.field.mediaType == "on") {
                data.field.mediaType = "01";
            } else {
                data.field.mediaType = "00";
            }

            $.getJsonData("/hycj/users", data.field, {type: "Put"}).done(function (data) {
                if (data.code != "200") {
                    layer.msg(data.message, {icon: 2});
                } else {
                    layer.msg('操作成功', {icon: 6});
                    layer.closeAll();
                    table.refresh();
                }

                processing = false;
            }).fail(function () {
                layer.msg("更新失败", {icon: 2});
                processing = false;
            });
            return false;
        });
    });
})(jQuery);