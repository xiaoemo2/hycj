(function ($) {
    var page = "app-address";
    layui.use(['element', 'form', 'layer', 'laytpl'], function () {
        var model = $(".app-address-content");
        var layer = layui.layer;
        var form = layui.form;
        var laytpl = layui.laytpl;

        //富文本编辑器
        var platformTypes = [
            {code: "ANDROID_APP_ADDRESS", description: "安卓"},
            {code: "IOS_APP_ADDRESS", description: "IOS"}
        ];

        var table = $(".app-address-table").table({
            columns: [
                {
                    "name": "keyNo",
                    "display": "平台类型",
                    render: function (rowData) {
                        var description = "未知类型";
                        $.each(platformTypes, function (index, e) {
                            if (rowData.keyNo == e.code) {
                                description = e.description;
                                return false;
                            }
                        });
                        return description;
                    }
                },
                {
                    "name": "keyValue",
                    "display": "APP地址"
                },
                {
                    "name": "caption",
                    "display": "说明信息"
                },
                {
                    "display": "操作",
                    render: function (rowData) {
                        console.log(rowData)
                        return "<div class='opt-btn-group'><span class='edit-item'><i class=\"layui-icon\">&#xe642;</i>编辑</span>" +
                            "</div>";
                    }
                }
            ],
            url: "/hycj/app-addresses",
            showTitle: false,
            formatData: function (data) {
                return data.datas;
            },
            params: {
                channelId: 3
            }
        });

        //编辑对话框
        model.on("click", ".edit-item", function (e) {
            var rowData = table.getRowData($(e.target).closest("tr"));
            laytpl($("#appAddressForm").html()).render({
                model: "updateAppAddressForm",
                data: rowData,
                platformTypes: platformTypes
            }, function (content) {
                layer.open({
                    type: 1,
                    title: "更新APP下载地址",
                    content: content,
                    area: ["600px"],
                    success: function () {
                        form.render();
                    }
                });
            });
        });

        //更新
        form.on('submit(updateAppAddressForm)', function (data) {
            $.getJsonData("/hycj/app-addresses", data.field, {type: "Put"}).done(function (data) {
                if (data.code != 200) {
                    layer.msg(data.message, {icon: 2});
                } else {
                    app.showSuccess();
                    table.refresh();
                }
            }).fail(function () {
            });
            return false;
        });

    });
})(jQuery);