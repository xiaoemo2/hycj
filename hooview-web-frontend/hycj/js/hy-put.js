(function ($) {
    var page = "hy-put";
    layui.use(['element', 'form', 'layer', 'laytpl', 'laydate'], function () {
        var model = $(".hy-put-content");
        var layer = layui.layer;
        var form = layui.form;
        var laytpl = layui.laytpl;
        var laydate = layui.laydate;
        //是否有任务在执行
        var processing = false;

        var table = $(".hy-put-table").table({
            columns: [
                {
                    "name": "hy",
                    "display": "投放量"
                },
                {
                    "name": "radix",
                    "display": "基数"
                },
                {
                    "name": "createTime",
                    "display": "投放时间"
                },
                {
                    "display": "操作",
                    render: function (rowData) {
                        var createTime = rowData.createTime;
                        if (!moment(createTime).isBefore(moment(moment().format("YYYY-MM-DD")))) {
                            return "<div class='opt-btn-group'><span class='edit-item'><i class=\"layui-icon\">&#xe642;</i>编辑</span>" +
                                "<span class='delete-item'><i class=\"layui-icon\">&#xe640;</i>删除</span></div>";
                        }
                        return "<span>无可用操作</span>";
                    }
                }
            ],
            url: "/hycj/hy-puts",
            showTitle: false,
            formatData: function (data) {
                return data.datas;
            }
        });

        //编辑对话框
        model.on("click", ".edit-item", function (e) {
            var rowData = table.getRowData($(e.target).closest("tr"));
            laytpl($("#hyPutForm").html()).render({
                model: "updateHYPutForm",
                data: rowData
            }, function (content) {
                layer.open({
                    type: 1,
                    title: "更新HY投放",
                    content: content,
                    area: "600px",
                    maxHeight: app.clientHeight() - 50,
                    success: function () {
                        laydate.render({
                            elem: '[name="createTime"]'
                            , type: 'date'
                            , format: 'yyyy-MM-dd'
                            , min: 0
                        })
                    }
                });
            });
        });

        //创建对话框
        model.on("click", ".create-item", function (e) {
            laytpl($("#hyPutForm").html()).render({
                model: "addHYPutForm",
                data: {}
            }, function (content) {
                layer.open({
                    type: 1,
                    title: "创建HY投放",
                    content: content,
                    area: "600px",
                    maxHeight: app.clientHeight() - 50,
                    success: function () {
                        laydate.render({
                            elem: '[name="createTime"]'
                            , type: 'date'
                            , format: 'yyyy-MM-dd'
                            , min: 0
                        })
                    }
                });
            });
        });

        //删除对话框
        model.on("click", ".delete-item", function (e) {
            var layer = layui.layer;
            var selectedData = table.getRowData($(e.target).closest("tr"));
            layer.msg('您确认删除' + selectedData.createTime + "对应的规则", {
                time: 5000, //5s后自动关闭
                btn: ['确认', '取消']
                , btn1: function (index, layero) {
                    $.getJsonData("/hycj/hy-puts"
                        , {id: selectedData.id}
                        , {type: "Delete"})
                        .done(function (ret) {
                            if (ret.datas === false) {
                                layer.msg("不能删除已经使用过的投放规则");
                            } else {
                                layer.msg("删除完成");
                                table.refresh();
                            }
                        }).fail(function () {
                        layer.msg("删除失败, 请稍后重试!");
                    });
                }
                , btn2: function (index, layero) {
                    layer.close(index);
                }
            });
        });

        //搜索
        form.on('submit(searchHYPutForm)', function (data) {
            if (!data.form) {
                return false
            }
            table.refresh(data.field);
            return false;
        });

        //新增
        form.on('submit(addHYPutForm)', function (data) {
            if (processing || !data.form) {
                return false
            }
            processing = true;

            //当前配置的天数
            var createTime = data.field.createTime;
            var formData = data.field;
            $.getJsonData("/hycj/hy-puts", formData, {type: "Post"}).done(function (saveResult) {
                if (saveResult.datas === false) {
                    layer.msg("已有" + createTime + "时间的投放", {icon: 2});
                    processing = false;
                } else if (saveResult.code != "200") {
                    layer.msg(saveResult.message, {icon: 2});
                    processing = false;
                } else {
                    layer.msg('操作成功', {icon: 6});
                    layer.closeAll();
                    table.refresh();
                }
                processing = false;
            }).fail(function () {
                processing = false;
            });

            return false;
        });

        //更新
        form.on('submit(updateHYPutForm)', function (data) {
            if (processing || !data.form) {
                return false
            }
            processing = true;
            var createTime = data.field.createTime;
            $.getJsonData("/hycj/hy-puts", data.field, {type: "Put"}).done(function (data) {
                if (data.datas === false) {
                    layer.msg("已有" + createTime + "时间的投放", {icon: 2});
                    processing = false;
                } else if (data.code != "200") {
                    layer.msg(data.message, {icon: 2});
                    processing = false;
                } else {
                    app.showSuccess();
                    table.refresh();
                    processing = false;
                }
            }).fail(function () {
                processing = false;
            });
            return false;
        });

        form.verify({
            integer: function (value, item) {
                if (value) {
                    try {
                        if (parseInt(value) != value) {
                            return "请输入整数";
                        }
                    } catch (error) {
                        return "请输入整数";
                    }
                }
            },
            positiveInteger: function (value, item) {//正整数
                if (value) {
                    try {
                        if (parseInt(value) != value || value <= 0) {
                            return "请输入正整数";
                        }
                    } catch (error) {
                        return "请输入正整数";
                    }
                }
            }
        });
    });
})(jQuery);