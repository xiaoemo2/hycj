(function ($) {
    var shareBGUrl = "/hycj/managements/share-bgimages";

    /**
     * 分享底图持有对象字典
     * @type {{}}
     */
    var shareBGImage = {};

    /**
     * 底图图片数组
     * @type {any[]}
     */
    var images;

    /**
     * 当前所修改的图片索引
     * @type {number}
     */
    var index = 0;

    /**
     * 页面载入时进行初始化操作
     */
    (function pageLoad() {
        $.getJsonData(shareBGUrl, {}, {type: "GET"})
            .done(function (result) {
                console.log(result);
                if (result && "200" == result["code"]) {
                    shareBGImage = result["datas"];
                    if (shareBGImage) {
                        var bgimage = shareBGImage.keyValue;
                        images = bgimage.split(";");
                        if (images && images.length > 0) {
                            var box = $(".images-container");
                            box.find(".block-image-left").attr("src", images[0]);
                            box.find(".block-image-right").attr("src", images[1]);
                        }
                    }
                }
            }).fail(function () {
            console.log("获取底图照片失败..");

        });
    })();

    /**
     * 上传图片
     */
    var uplodImage = function () {
        moon.uploader({
            callback: function (response) {
                console.log(response);
                if (response && response.length > 0) {
                    configImages(response[0]);
                }
            }
        });
    };

    /**
     * 设置配置底图图片
     * @param images
     */
    var configImages = function (image) {
        var box = $(".images-container");
        images = images || new Array();
        images[index] = image["path"];
        box.find(".block-image-left").attr("src", images[0]);
        box.find(".block-image-right").attr("src", images[1]);
        updateImages();
    };

    /**
     * 更新分享底图图片
     */
    var updateImages = function () {
        console.log(images);
        shareBGImage = shareBGImage || {};
        if (images) {
            shareBGImage["keyValue"] = images.join(";");
        }
        $.getJsonData(shareBGUrl, shareBGImage, {type: "PUT"})
            .done(function (result) {
                console.log(result);
                if (result && "200" == result["code"] && result["datas"]) {
                    app.showSuccess("保存底图图片成功");
                } else {
                    app.showError("保存底图图片失败，请重试!");
                }
            }).fail(function () {
            app.showError("保存底图图片失败，请重试!");
        });
    };

    /**
     * 获取当前底图图片
     * @returns {any[]}
     */
    var getImages = function () {
        var box = $(".images-container");
        var images = new Array();
        images[0] = box.find(".block-image-left").attr("src");
        images[1] = box.find(".block-image-right").attr("src");
        return images;
    };

    /**
     * 点击上传文件按钮，创建上传文件对话框
     */
    $(".grid-container").on("click", ".change-image-left", function () {
        index = 0;
        uplodImage();
    });

    /**
     * 点击上传文件按钮，创建上传文件对话框
     */
    $(".grid-container").on("click", ".change-image-right", function () {
        index = 1;
        uplodImage();
    });

})(jQuery);