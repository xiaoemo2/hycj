(function ($) {
    var page = "sign-rule";
    layui.use(['element', 'form', 'layer', 'laytpl'], function () {
        var model = $(".sign-rule-content");
        var layer = layui.layer;
        var form = layui.form;
        var laytpl = layui.laytpl;

        //是否有任务在执行
        var processing = false;

        var table = $(".sign-rule-table").table({
            columns: [
                {
                    "name": "signDays",
                    "display": "连续签到天数"
                },
                {
                    "name": "rewardCoins",
                    "display": "奖励糖果数"
                },
                // {
                //     "name": "expireTime",
                //     "display": "过期时间",
                //     "render": function (rowData) {
                //         rowData.expireTimeInHour = (rowData.expireTime / 60).toFixed(0);
                //         return rowData.expireTimeInHour + "小时";
                //     }
                // },
                {
                    "display": "操作",
                    render: function (rowData) {
                        return "<div class='opt-btn-group'><span class='edit-item'><i class=\"layui-icon\">&#xe642;</i>编辑</span>" +
                            "<span class='delete-item'><i class=\"layui-icon\">&#xe640;</i>删除</span></div>";
                    }
                }
            ],
            url: "/hycj/sign-rules",
            showTitle: false,
            formatData: function (data) {
                return data.datas;
            }
        });

        //编辑对话框
        model.on("click", ".edit-item", function (e) {
            var rowData = table.getRowData($(e.target).closest("tr"));
            laytpl($("#signRuleForm").html()).render({
                model: "updateSignRuleForm",
                data: rowData
            }, function (content) {
                layer.open({
                    type: 1,
                    title: "更新签到规则",
                    content: content,
                    area: "600px",
                    maxHeight: app.clientHeight() - 50
                });
            });
        });

        //创建对话框
        model.on("click", ".create-item", function (e) {
            laytpl($("#signRuleForm").html()).render({
                model: "addSignRuleForm",
                data: {}
            }, function (content) {
                layer.open({
                    type: 1,
                    title: "创建签到规则",
                    content: content,
                    area: "600px",
                    maxHeight: app.clientHeight() - 50
                });
            });
        });

        //删除对话框
        model.on("click", ".delete-item", function (e) {
            var layer = layui.layer;
            var selectedData = table.getRowData($(e.target).closest("tr"));
            layer.msg('您确认删除<br>连续签到' + selectedData.signDays + "天对应的规则", {
                time: 5000, //5s后自动关闭
                btn: ['确认', '取消']
                , btn1: function (index, layero) {
                    $.getJsonData("/hycj/sign-rules"
                        , {id: selectedData.id}
                        , {type: "Delete"})
                        .done(function () {
                            layer.msg("删除完成");
                            table.refresh();
                        }).fail(function () {
                        layer.msg("删除失败, 请稍后重试!");
                    });
                }
                , btn2: function (index, layero) {
                    layer.close(index);
                }
            });
        });

        //搜索
        form.on('submit(searchSignRuleForm)', function (data) {
            if (!data.form) {
                return false
            }
            table.refresh(data.field);
            return false;
        });

        //新增
        form.on('submit(addSignRuleForm)', function (data) {
            if (processing || !data.form) {
                return false
            }
            processing = true;

            //当前配置的天数
            var signDays = data.field.signDays;
            var formData = data.field;
            $.getJsonData("/hycj/sign-rules/get-by-sign-days", {signDays: signDays}, {type: "Get"}).done(function (data) {
                if (data.datas) {//存在
                    layer.msg("连续签到" + signDays + "天的规则已经存在", {icon: 2});
                    processing = false;
                } else {
                    $.getJsonData("/hycj/sign-rules", formData, {type: "Post"}).done(function (saveResult) {
                        if (saveResult.code != "200") {
                            layer.msg("规则保存异常，请稍后再试", {icon: 2});
                        } else {
                            layer.msg('操作成功', {icon: 6});
                            layer.closeAll();
                            table.refresh();
                        }
                        processing = false;
                    }).fail(function () {
                        processing = false;
                    });
                }
            }).fail(function () {
                processing = false;
            });
            return false;
        });

        //更新
        form.on('submit(updateSignRuleForm)', function (data) {
            if (processing || !data.form) {
                return false
            }
            processing = true;

            $.getJsonData("/hycj/sign-rules", data.field, {type: "Put"}).done(function (data) {
                app.showSuccess();
                table.refresh();
                processing = false;
            }).fail(function () {
                processing = false;
            });
            return false;
        });

        form.verify({
            integer: function (value, item) {
                if (value) {
                    try {
                        if (parseInt(value) != value) {
                            return "请输入整数";
                        }
                    } catch (error) {
                        return "请输入整数";
                    }
                }
            },
            positiveInteger: function (value, item) {//正整数
                if (value) {
                    try {
                        if (parseInt(value) != value || value <= 0) {
                            return "请输入正整数";
                        }
                    } catch (error) {
                        return "请输入正整数";
                    }
                }
            }
        });
    });
})(jQuery);