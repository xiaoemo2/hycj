(function ($) {
    layui.use(['element', 'form', 'laytpl'], function () {
        var element = layui.element;
        var form = layui.form;
        var laytpl = layui.laytpl;

        //左侧菜单点击事件
        element.on('nav(menu)', function (data) {
            var $aTag = $("a", data[0]);
            var menuURI = $aTag.attr("href");
            //移除第一个#号
            menuURI = menuURI.substring(1);
            if (menuURI == "") {
                layui.layer.msg("菜单暂未实现");
                return;
            }
            app.showPage(menuURI, $aTag.html(), true);
        });

        //顶部点击事件
        element.on('nav(top-menu)', function (data) {
            var $a = $("a", data);
            var action = $a.attr("data-action");
            if (action == "logout") {
                //登出
                $.getJsonData("/hycj/users/logout", {}, {type: "Post"});
            } else if (action == "refresh") {
                //刷新tab
                app.refreshSelectedTabContent();
            } else if (action == "resetPassword") {
                //修改密码
                laytpl($("#resetPasswordForm").html()).render({}, function (content) {
                    layer.open({
                        type: 1,
                        title: "修改密码",
                        content: content,
                        area: ["600px"]
                    });
                });
            }
            return false;
        });

        /**
         * 修改密码
         */
        form.on('submit(resetPasswordForm)', function (data) {
            if (!data.form) {
                return false
            }
            $.getJsonData("/hycj/users/password", {
                oldPassword: data.field.oldPassword,
                newPassword: data.field.newPassword
            }, {type: "Put"}).done(function (data) {
                if (data.code == '200') {
                    layer.alert("密码修改成功", {icon: 6}, function () {
                        $.getJsonData("/hycj/users/logout", {}, {type: "Post"});
                    });
                } else {
                    layer.msg(data.message, {icon: 2});
                }
            });
            return false;
        });

        form.verify({
            rePassword: function (value, item) {
                var password = $(item).closest("form").find("[data-id='rePassword']").val();
                if (password !== value) {
                    return "两次密码输入不一致";
                }
            }
        });

        /**
         * 反向同步tab选中状态到菜单上
         */
        element.on('tab(moon-tab)', function (data) {
            var layId = $(this).attr("lay-id");
            $("a", "[lay-filter='menu']").each(function (index, aTag) {
                if ($.hashCode($(aTag).html()) == layId) {
                    $("[lay-filter='menu'] .layui-this").removeClass("layui-this");
                    $(this).closest("li").addClass("layui-this");
                }
            });
        });

        //该部分逻辑只会在页面首次打开的时候执行一次，用于处理根据url打开tab
        //真实打开的url
        var actualUri = window.location.hash;
        $(".layui-nav-item a").each(function (index, e) {
            if (e.getAttribute("href") == actualUri) {
                $(e).trigger("click");
            }
        });

        saveTicketIntoCookieFromUrl();
        fillUserInfo();

        //如果没有打开任何菜单
        if ($("[lay-filter='menu'] .layui-nav-item.layui-this").length == 0) {
            //默认打开第一个菜单
            $("[lay-filter='menu'] .layui-nav-item:eq(0)").click();
        }
    });

    function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg); //匹配目标参数
        if (r != null) return unescape(r[2]);
        return null; //返回参数值
    }

    /**
     * 从url上获取ticket并存在到cookie中
     */
    function saveTicketIntoCookieFromUrl() {
        var url = window.location.href;
        var ticket = getUrlParam("ticket");
        if (ticket) {
            $.cookie("ticket", ticket);
            window.location.href = window.location.href.replace(window.location.search, "")
        }
    }

    /**
     * 填充用户信息
     */
    function fillUserInfo() {
        $.getJsonData("/hycj/users/current").done(function (ret) {
            var user = ret.datas;
            $("#userInfo").html(//"<img src=\"/tf/file/get?filePath=" + user.avatar + "\" class=\"layui-nav-img\">\n" +
                user.username || user.email);
        });
    }

    $(function () {
        //textarea字数统计和限制
        $(document).on("input propertychange", ".textarea-limit", function (e) {
            //已经输入长度
            var inputLength = $(e.target).val().trim().length;
            if (inputLength > 500) {
                $(e.target).val($(e.target).val().substr(0, 500));
                $(".textarea-status").html("500/500");
            } else {
                $(".textarea-status").html(inputLength + "/500");
            }
        });

        /**
         * 取消重置密码
         */
        $(document).on("click", ".cancel-reset-password", function () {
            layui.layer.closeAll();
        });
    });
})(jQuery);